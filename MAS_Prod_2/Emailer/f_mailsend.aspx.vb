﻿Imports System.Net.Mail
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Data
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html.simpleparser
Imports SelectPdf
Imports MySql.Data.MySqlClient

Public Class f_mailsend
    'Inherits System.Web.UI.Page
    Inherits BaseClass

    Dim conn As New MySqlConnection(ConfigurationManager.ConnectionStrings("MASConnectionString").ConnectionString())

    Dim dt As Date, Last_Month As String, This_Month As String, Y As String, Prev_Day As String, strBody As String
    Dim temp_date As Date

    Dim start_num As Integer = 1
    Dim end_num As Integer = 31
    Dim month_year As String = "2018-01"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'SendEmailMonthly()
        sendEmailPDF(getDate(DateAdd(DateInterval.Day, -1, Now)))
        sendEmail(getDate(DateAdd(DateInterval.Day, -1, Now)))
    End Sub

    Sub SendEmailMonthly()
        Dim strReq As String

        Dim conString = ConfigurationManager.ConnectionStrings("MASConnectionString")
        Dim strConnString As String = conString.ConnectionString
        Dim conn As New MySqlConnection(strConnString)
        Dim str_path As String = HttpContext.Current.Request.Url.Host
        Dim strbody As String = Nothing

        Dim message As New MailMessage()

        message.From = New MailAddress("no_reply_mas_system@baramultigroup.co.id", "Alert MAS System")
        message.Subject = "MAS Business Information"

        For i = start_num To end_num
            temp_date = month_year & "-" & Right("0" + i.ToString(), 2)

            Prev_Day = temp_date.ToString("yyyy-MM-dd")
            Last_Month = DateAdd(DateInterval.Month, -1, temp_date).ToString("yyyyMM")
            This_Month = temp_date.ToString("yyyyMM")
            Y = temp_date.ToString("yyyy")
            strReq = "?MDateT=" + Prev_Day + "&LM=" + Last_Month + "&TM=" + This_Month + "&Y=" + Y

            Dim converter As HtmlToPdf = New HtmlToPdf()
            converter.Options.MaxPageLoadTime = 300
            converter.Options.PdfPageSize = PdfPageSize.A4
            converter.Options.MarginTop = 20

            converter.Options.MarginLeft = 25
            converter.Options.MarginRight = 10
            converter.Options.MarginBottom = 10
            Dim doc As SelectPdf.PdfDocument = converter.ConvertUrl("http://www.baramultigroup.co.id/ProductionMAS/Emailer/r_BIDB.aspx" + strReq)
            'Dim doc As SelectPdf.PdfDocument = converter.ConvertUrl("http://localhost:61647/Emailer/r_BIDB.aspx" + strReq)
            Dim pdfstream As New MemoryStream
            doc.Save(pdfstream)
            pdfstream.Position = 0
            message.Attachments.Add(New Attachment(pdfstream, "MAS_BusinessInformation_" + temp_date + ".pdf"))

            doc.Close()
        Next

        'message.To.Add("edwin_k@baramultigroup.co.id")
        'message.To.Add("sutini@baramultigroup.co.id")
        message.Bcc.Add("yustine_a@baramultigroup.co.id")

        strbody = "Dear Officer, <br /> <br />"
        strbody += "MAS Production Dashboard, enclosed with this email. <br /> <br />"
        strbody += "Have a nice day,<br /> <br />"
        strbody += "MAS (System) <br /> <br /> <br />"
        strbody += "DISCLAIMER: <br />"
        strbody += "The information contained in this communication is intended solely for the use of the individual or entity to whom it is addressed and others authorized to receive it. It may contain confidential or legally privileged information. If you are not the intended recipient you are hereby notified that any disclosure, copying, distribution or taking any action in reliance on the contents of this information is strictly prohibited and may be unlawful. Unless otherwise specifically stated by the sender, any documents or views presented are solely those of the sender and do not constitute official documents or views of Muara Alam Sejahtera, PT." & _
                    "If you have received this communication in error, please notify us immediately by responding to this e-mail and then deleting it from your system. Muara Alam Sejahtera, PT. is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay in its receipt." & _
                    "Muara Alam Sejahtera, PT. also accepts no liability for any virus or other harmful effects that may ensue as a result from the opening of this e-mail. -MAS"

        message.Body = strbody
        message.IsBodyHtml = True

        Dim smtp = New SmtpClient("mail.baramultigroup.co.id")
        smtp.Send(message)
    End Sub

    Protected Function getDate(ByVal dt As Date) As String
        Dim strReq As String
        Prev_Day = dt.ToString("yyyy-MM-dd")
        Last_Month = DateAdd(DateInterval.Month, -1, dt).ToString("yyyyMM")
        This_Month = dt.ToString("yyyyMM")
        Y = dt.ToString("yyyy")
        strReq = "?MDateT=" + Prev_Day + "&LM=" + Last_Month + "&TM=" + This_Month + "&Y=" + Y
        Return strReq
    End Function

    Protected Sub sendEmailPDF(ByVal strdate As String)
        Dim str_path As String = HttpContext.Current.Request.Url.Host
        Dim strbody As String = Nothing
        'Dim dt As DateTime = CDate(Session("iframe")).ToString("dd/MM/yyyy")

        Dim converter As HtmlToPdf = New HtmlToPdf()
        converter.Options.MaxPageLoadTime = 300
        converter.Options.PdfPageSize = PdfPageSize.A4
        converter.Options.MarginTop = 20

        converter.Options.MarginLeft = 25
        converter.Options.MarginRight = 10
        converter.Options.MarginBottom = 10
        Dim doc As SelectPdf.PdfDocument = converter.ConvertUrl("http://www.baramultigroup.co.id/ProductionMAS/Emailer/r_BIDB.aspx" + strdate)
        'Dim doc As SelectPdf.PdfDocument = converter.ConvertUrl("~/r_BIDB.aspx" + strdate)
        Dim pdfstream As New MemoryStream
        doc.Save(pdfstream)
        pdfstream.Position = 0

        'Dim sw = New StringWriter
        'Dim htw = New HtmlTextWriter(sw)
        'Server.Execute("r_BIDB.aspx" + strdate, htw)
        'Dim sr As New StringReader(sw.ToString())

        Dim message As New MailMessage()

        message.From = New MailAddress("no_reply_mas_system@baramultigroup.co.id", "Alert MAS System")
        message.Subject = "MAS Business Information"
        message.Attachments.Add(New Attachment(pdfstream, "MAS_BusinessInformation_" + DateAdd(DateInterval.Day, -1, Now).ToString("yyyy-MM-dd") + ".pdf"))

        GetRecipient(message, "MASDBOARD_PDF")
        'GetRecipient(message, "TESTME")

        strbody = "Dear Officer, <br /> <br />"
        strbody += "MAS Production Dashboard, enclosed with this email. <br /> <br />"
        strbody += "Have a nice day,<br /> <br />"
        strbody += "MAS (System) <br /> <br /> <br />"
        strbody += "DISCLAIMER: <br />"
        strbody += "The information contained in this communication is intended solely for the use of the individual or entity to whom it is addressed and others authorized to receive it. It may contain confidential or legally privileged information. If you are not the intended recipient you are hereby notified that any disclosure, copying, distribution or taking any action in reliance on the contents of this information is strictly prohibited and may be unlawful. Unless otherwise specifically stated by the sender, any documents or views presented are solely those of the sender and do not constitute official documents or views of Muara Alam Sejahtera, PT." & _
                    "If you have received this communication in error, please notify us immediately by responding to this e-mail and then deleting it from your system. Muara Alam Sejahtera, PT. is neither liable for the proper and complete transmission of the information contained in this communication nor for any delay in its receipt." & _
                    "Muara Alam Sejahtera, PT. also accepts no liability for any virus or other harmful effects that may ensue as a result from the opening of this e-mail. -MAS"

        message.Body = strbody
        message.IsBodyHtml = True

        doc.Close()

        'Dim smtp = New SmtpClient("mail.baramultigroup.co.id")
        'smtp.Send(message)

        'update by yustine 07/09/2020
        Dim smtp As New SmtpClient()
        GetSMTPData(smtp)
        smtp.Send(message)
    End Sub

    Sub GetRecipient(ByVal message As MailMessage, ByVal notification As String)
        Dim sqlCmd As MySqlCommand
        Dim sqlReader As MySqlDataReader
        conn.Open()
        sqlCmd = New MySqlCommand("select Ref, Email, Fullname from mn_notification where Notification = '" & notification & "' and Active = 1", conn)
        sqlReader = sqlCmd.ExecuteReader

        Do While sqlReader.Read
            Select Case sqlReader(0).ToString
                Case "CC"
                    message.CC.Add(New MailAddress(sqlReader(1).ToString, sqlReader(2).ToString))
                Case "BCC"
                    message.Bcc.Add(New MailAddress(sqlReader(1).ToString, sqlReader(2).ToString))
                Case Else
                    message.To.Add(New MailAddress(sqlReader(1).ToString, sqlReader(2).ToString))
            End Select
        Loop
        sqlReader.Close()
        conn.Close()
    End Sub


    Protected Sub sendEmail(ByVal strdate As String)
        'Dim sqlCmd As MySqlCommand
        'Dim sqlReader As MySqlDataReader
        'Dim str_path As String = HttpContext.Current.Request.Url.Host

        'Dim dt As DateTime = CDate(Session("iframe")).ToString("dd/MM/yyyy")

        Dim sw = New StringWriter
        Dim htw = New HtmlTextWriter(sw)
        Server.Execute("r_BIDB.aspx" + strdate, htw)
        'Server.Execute("http://www.baramultigroup.co.id/ProductionMAS/Emailer/r_BIDB.aspx" + strdate, htw)

        Dim message As New MailMessage()

        message.From = New MailAddress("no_reply_mas_system@baramultigroup.co.id", "Alert MAS System")
        message.Subject = "MAS Business Information"

        GetRecipient(message, "MASDBOARD")
        'GetRecipient(message, "TESTME")

        message.Body = sw.ToString()
        message.IsBodyHtml = True

        'update by yustine 07/09/2020
        Dim smtp As New SmtpClient()
        GetSMTPData(smtp)
        smtp.Send(message)
    End Sub

    'Public Function GetSMTPData(ByVal smtp As SmtpClient) As SmtpClient
    '    conn.Open()
    '    Dim host As String = ""
    '    Dim email_from As String = ""
    '    Dim password As String = ""
    '    Dim port As Integer
    '    Dim oDatareader As MySqlDataReader
    '    Dim sqlCmd_host As New MySqlCommand("select * from mn_code_list where CatID = 'EMAIL' and Code = 'HOST' and Active = 1 ", conn)
    '    oDatareader = sqlCmd_host.ExecuteReader()
    '    If oDatareader.Read() Then
    '        host = oDatareader("Description").ToString()
    '    End If
    '    oDatareader.Close()
    '    Dim sqlCmd_port As New MySqlCommand("select * from mn_code_list where CatID = 'EMAIL' and Code = 'PORT' and Active = 1 ", conn)
    '    oDatareader = sqlCmd_port.ExecuteReader()
    '    If oDatareader.Read() Then
    '        port = Integer.Parse(oDatareader("Description").ToString())
    '    End If
    '    oDatareader.Close()
    '    Dim sqlCmd_email_from As New MySqlCommand("select * from mn_code_list where CatID = 'EMAIL' and Code = 'FROM' and Active = 1 ", conn)
    '    oDatareader = sqlCmd_email_from.ExecuteReader()
    '    If oDatareader.Read() Then
    '        email_from = oDatareader("Description").ToString()
    '    End If
    '    oDatareader.Close()
    '    Dim sqlCmd_email_pass As New MySqlCommand("select * from mn_code_list where CatID = 'EMAIL' and Code = 'PASS' and Active = 1 ", conn)
    '    oDatareader = sqlCmd_email_pass.ExecuteReader()
    '    If oDatareader.Read() Then
    '        password = oDatareader("Description").ToString()
    '    End If
    '    oDatareader.Close()
    '    conn.Close()

    '    smtp.Host = host
    '    smtp.Port = port
    '    smtp.Credentials = New System.Net.NetworkCredential(email_from, password)
    '    smtp.EnableSsl = True
    '    Return smtp
    'End Function
End Class