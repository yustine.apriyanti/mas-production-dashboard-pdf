﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="r_BIDB.aspx.vb" Inherits="MAS_Prod_2.r_BIDB" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">

        .style1
        {
            color: #FFFFFF;
        }
            
        .style7
        {
            color: #FFFFFF;
        }
        .style8
        {
            text-align: center;
        }
        .style9
        {
            text-align: left;
        }
        .style10
        {
            color: #FFFFFF;
            text-align: left;
        }
        .style11
        {
            color: #FFFFFF;
            text-align: right;
        }
        .style12
        {
            width: 89%;
        }
        .style13
        {
            text-align: right;
        }
        .style15
        {
            font-size: 9pt;
        }
        .style2
        {
            text-align: center;
        }
        .style16
        {
            width: 680px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <table border="0" cellpadding="0px" cellspacing="0px" 
        style="height:auto; text-align: center; width: 28.9cm;">
        <tr valign="top">
            <td colspan="2">
                <div style="text-align:center; height:40px; width: 1052px;">
                    <h1 style="width: 1052px">
                    <asp:Label ID="lblHeader" runat="server" Text="MAS BUSINESS INFORMATION" ForeColor="#666666"></asp:Label></h1>
                </div>
                <div style="text-align:center; height:50px; width: 1057px;">
                    <br />
                    <asp:Label ID="lblDate" runat="server" Text="Label" ForeColor="#666666" style="font-weight: 700"></asp:Label><br />
                </div>
            </td>
       </tr>
    </table>

    <table border="0" cellpadding="5px" cellspacing="0px" style="height:auto; text-align: center; width: 21cm;">
        <tr>
            <td valign="top">
                <asp:DataList ID="DLProduction" runat="server" BackColor="White" 
                    BorderColor="#555555" Width="520px" 
                    BorderStyle="None" BorderWidth="2px" CellPadding="5" DataSourceID="DSProduction" 
                    ForeColor="Black" GridLines="Vertical" 
                    style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt">
                    <AlternatingItemStyle BackColor="#555555" />
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="WhiteSmoke" Font-Bold="True" ForeColor="Red" Font-Size="12pt" Height="20px" />
                    <HeaderTemplate>
                        <asp:Label ID="Label4" runat="server" Font-Size="12pt">Production Alam 1,2,3</asp:Label>
                    </HeaderTemplate>
                    <ItemStyle BackColor="#555555" Font-Size="9pt" />
                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <ItemTemplate>
                        <table cellpadding="0px" style="padding: 0px 10px 0px 10px; background-color: #555555" align="center" cellspacing="0">
                            <tr>
                                <td align="center" valign="middle" width="100px" bgcolor="#555555">
                                    <span class="style1">Contractor:</span><br />
                                    <asp:Label ID="CategoryIDLabel" runat="server" 
                                        Text='<%# Eval("Contractor_Id") %>' Font-Size="10pt" 
                                        Font-Bold="True" ForeColor="White">
                                    </asp:Label>
                                </td>
                                <td>
                                    <table border="0" cellpadding="0px" cellspacing="0">
                                        <tr>
                                            <td width="100px" style="font-weight: bold; background-color: #555555; color: #FFFFFF; 
                                                font-family: Arial, Helvetica, sans-serif; padding-top: 3px; padding-bottom: 3px;" bgcolor="#555555" class="style9">
                                                Coal Getting
                                            </td>
                                            <td width="90px" style="font-weight: bold; background-color: #555555; color: #FFFFFF; 
                                                font-family: Arial, Helvetica, sans-serif;" bgcolor="#555555" class="style13">
                                                DAILY
                                            </td>
                                            <td width="90px" style="font-weight: bold; background-color: #555555; color: #FFFFFF; 
                                                font-family: Arial, Helvetica, sans-serif;" bgcolor="#555555" class="style13">
                                                MTD
                                            </td>
                                            <td width="90px" style="font-weight: bold; background-color: #555555; color: #FFFFFF; 
                                                font-family: Arial, Helvetica, sans-serif;" bgcolor="#555555" class="style13">
                                                YTD
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; padding-left: 5px; padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="White" class="style9">
                                                Target
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label1" runat="server" Text='<%# string.Format("{0:n2}",Eval("Plan_Daily_CP")) %>'></asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label2" runat="server" Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_CP")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label3" runat="server" Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_CP")) %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; padding-left: 5px; padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="White" class="style9">
                                                Actual
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label85" runat="server" Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_CP")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label5" runat="server" Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_CP")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label6" runat="server" Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_CP")) %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; padding-left: 5px; padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="White" class="style9">
                                                Percentage
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label26" runat="server" Font-Bold="True" Width="100%"></asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label27" runat="server" Font-Bold="True" Width="100%"></asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label28" runat="server" Font-Bold="True" Width="100%"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; background-color: #555555; color: #FFFFFF; 
                                                font-family: Arial, Helvetica, sans-serif; padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="#555555" class="style9">
                                                OB Removal
                                            </td>
                                            <td width="90px" 
                                                style="font-weight: bold; background-color: #555555; color: #FFFFFF; font-family: Arial, Helvetica, sans-serif;" 
                                                bgcolor="#555555" class="style13">
                                                DAILY
                                            </td>
                                            <td width="90px" 
                                                style="font-weight: bold; background-color: #555555; color: #FFFFFF; font-family: Arial, Helvetica, sans-serif;" 
                                                bgcolor="#555555" class="style13">
                                                MTD
                                            </td>
                                            <td width="90px" style="font-weight: bold; background-color: #555555; color: #FFFFFF; font-family: Arial, Helvetica, sans-serif;" 
                                                bgcolor="#555555" class="style13">
                                                YTD
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; padding-left: 5px; padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="White" class="style9">
                                                Target
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="lblPlan_Daily" runat="server" Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_OB")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="lblPlan_Monthly" runat="server" Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_OB")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="lblPlan_Yearly" runat="server" Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_OB")) %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; padding-left: 5px; padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="White" class="style9">
                                                Actual
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="lblDaily_Actual" runat="server" Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_OB")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="lblMTD_Actual" runat="server" Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_OB")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="lblYTD_Actual" runat="server" Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_OB")) %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; padding-left: 5px; padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="White" class="style9">
                                                Percentage
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label29" runat="server" Font-Bold="True" Width="100%"></asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label30" runat="server" Font-Bold="True" Width="100%"></asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label31" runat="server" Font-Bold="True" Width="100%"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px"  style="font-weight: bold; padding-left: 5px; padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="White" class="style9">
                                                SR
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label82" runat="server" Text='<%#string.Format("{0:n2}",Eval("SRD")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label83" runat="server" Text='<%#string.Format("{0:n2}",Eval("SRM")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label84" runat="server" Text='<%#string.Format("{0:n2}",Eval("SRY")) %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <SeparatorStyle BackColor="WhiteSmoke" />
                    <SeparatorTemplate></SeparatorTemplate>
                </asp:DataList>
                <asp:DataList ID="DLProductionTotal" runat="server" BackColor="White" 
                    BorderColor="#555555" Width="520px" 
                    BorderStyle="None" CellPadding="5" DataSourceID="DSProductionTotal" 
                    ForeColor="Black" GridLines="Vertical" 
                    style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt" 
                    ShowFooter="False">
                    <AlternatingItemStyle BackColor="#555555" />
                    <FooterStyle BackColor="#E3DFDF" />
                    <FooterTemplate></FooterTemplate>
                    <HeaderStyle BackColor="#E3DFDF" Font-Bold="True" ForeColor="Red" Font-Size="10pt" Height="0px" />
                    <HeaderTemplate></HeaderTemplate>
                    <ItemStyle BackColor="#555555" Font-Size="9pt" />
                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <ItemTemplate>
                        <table style="padding: 0px 10px 0px 10px; background-color: #555555" align="center" cellspacing="0">
                            <tr>
                                <td align="center" valign="middle" width="100px" bgcolor="#555555">
                                    <asp:Label ID="lbltotal" runat="server" Text="Total" Font-Size="10pt" Font-Bold="True" ForeColor="White"></asp:Label><br />
                                    <asp:Label ID="lblMAS" runat="server" Text="Alam 1,2,3" Font-Size="10pt" Font-Bold="True" ForeColor="White"></asp:Label>
                                </td>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="100px" style="font-weight: bold; background-color: #555555; color: #FFFFFF;  padding-top: 3px; padding-bottom: 3px;" 
                                                class="style9">
                                                Coal Getting
                                            </td>
                                            <td width="90px" style="font-weight: bold; background-color: #555555; color: #FFFFFF;" class="style11">
                                                DAILY
                                            </td>
                                            <td width="90px" style="font-weight: bold; background-color: #555555; color: #FFFFFF;"\class="style11">
                                                MTD
                                            </td>
                                            <td width="90px" style="font-weight: bold; background-color: #555555; color: #FFFFFF; " class="style11">
                                                YTD
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="White" class="style9">
                                                Target
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label1" runat="server" Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_CP")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label2" runat="server" Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_CP")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label3" runat="server" Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_CP")) %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="White" class="style9">
                                                Actual
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label4" runat="server" Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_CP")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label5" runat="server" Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_CP")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label6" runat="server" Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_CP")) %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="White" class="style9">
                                                Percentage
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label32" runat="server" Font-Bold="True" Width="100%"></asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label33" runat="server"  Font-Bold="True" Width="100%"></asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label34" runat="server" Font-Bold="True" Width="100%"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; background-color: #555555; color: #FFFFFF;  padding-top: 3px; padding-bottom: 3px;" 
                                                class="style9">
                                                OB Removal
                                            </td>
                                            <td width="90px" style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                                                class="style11">
                                                DAILY
                                            </td>
                                            <td width="90px" style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                                                class="style11">
                                                MTD
                                            </td>
                                            <td width="90px" style="font-weight: bold; background-color: #555555; color: #FFFFFF; " 
                                                class="style11">
                                                YTD
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="White" class="style9">
                                                Target
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="lblPlan_Daily" runat="server" Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_OB")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="lblPlan_Monthly" runat="server" Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_OB")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="lblPlan_Yearly" runat="server" Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_OB")) %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="White" class="style9">
                                                Actual
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="lblDaily_Actual" runat="server" Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_OB")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="lblMTD_Actual" runat="server" Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_OB")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="lblYTD_Actual" runat="server" Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_OB")) %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="White" class="style9">
                                                Percentage
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label35" runat="server" Font-Bold="True" Width="100%"></asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label36" runat="server" Font-Bold="True" Width="100%"></asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label37" runat="server" Font-Bold="True" Width="100%"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                                                bgcolor="White" class="style9">
                                                SR
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label85" runat="server" Text='<%#string.Format("{0:n2}",Eval("SRD")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label86" runat="server" Text='<%#string.Format("{0:n2}",Eval("SRM")) %>'>
                                                </asp:Label>
                                            </td>
                                            <td width="90px" align="right" bgcolor="White">
                                                <asp:Label ID="Label87" runat="server" Text='<%#string.Format("{0:n2}",Eval("SRY")) %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <SeparatorStyle BackColor="WhiteSmoke" />
                    <SeparatorTemplate></SeparatorTemplate>
                </asp:DataList>
            </td>
            <td valign="top">
                <asp:DataList ID="DLProduction4" runat="server" BackColor="White" 
                                    BorderColor="#555555" Width="520px" 
                    BorderStyle="None" BorderWidth="2px" CellPadding="5" DataSourceID="DSProduction4" 
                    ForeColor="Black" GridLines="Vertical" 
                                    style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt">
                <AlternatingItemStyle BackColor="#555555" />
                <FooterStyle BackColor="#CCCC99" />
                <HeaderStyle BackColor="WhiteSmoke" Font-Bold="True" ForeColor="Red" Font-Size="12pt" 
                                        Height="20px" />
                <HeaderTemplate>
                    <asp:Label ID="Label4" runat="server" Font-Size="12pt">Production Alam 4</asp:Label>
                </HeaderTemplate>
                <ItemStyle BackColor="#555555" Font-Size="9pt" />
                <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <ItemTemplate>
            <table cellpadding="0px" style="padding: 0px 10px 0px 10px; background-color: #555555" align="center" cellspacing="0">
            <tr>
            <td align="center" valign="middle" width="100px" bgcolor="#555555">
                <span class="style1">Contractor:</span><br />
                            <asp:Label ID="CategoryIDLabel" runat="server" 
                                Text='<%# Eval("Contractor_Id") %>' Font-Size="10pt" 
                    Font-Bold="True" ForeColor="White"></asp:Label>
            </td>
            <td>
            <table border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td width="100px" 
        
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;  padding-top: 3px; padding-bottom: 3px;" 
                    class="style9">Coal Getting</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style13">DAILY</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style13">MTD</td>
            <td width="90px" 
        
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF; " 
                    class="style13">YTD</td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Target</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label1" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label2" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label3" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_CP")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Actual</td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label85" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label5" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label6" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_CP")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Percentage</td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label26" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label27" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label28" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
        
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;  padding-top: 3px; padding-bottom: 3px;" 
                    class="style9">OB Removal</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style13">DAILY</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style13">MTD</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF; " 
                    class="style13">YTD</td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Target</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblPlan_Daily" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblPlan_Monthly" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblPlan_Yearly" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_OB")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Actual</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblDaily_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblMTD_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblYTD_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_OB")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Percentage</td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label29" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label30" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label31" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">SR</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label82" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRD")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label83" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRM")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label84" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRY")) %>'>
                            </asp:Label></td>
            </tr>
            </table>
            </td>
            </tr>

            </table>
            </ItemTemplate>
                <SeparatorStyle BackColor="WhiteSmoke" />
                <SeparatorTemplate>
        
                </SeparatorTemplate>
            </asp:DataList>
                <asp:DataList ID="DLProductionTotal4" runat="server" BackColor="White" 
                                    BorderColor="#DEDFDE" Width="520px" 
                    BorderStyle="None" CellPadding="5" DataSourceID="DSProductionTotal4" 
                    ForeColor="Black" GridLines="Vertical" 
                                    style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt" 
                                    ShowFooter="False">
                <AlternatingItemStyle BackColor="#555555" />
                <FooterStyle BackColor="#E3DFDF" />
                <FooterTemplate></FooterTemplate>
                <HeaderStyle BackColor="#E3DFDF" Font-Bold="True" ForeColor="Red" Font-Size="12pt" Height="0px" />
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemStyle BackColor="#555555" Font-Size="9pt" />
                <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <ItemTemplate>
            <table style="padding: 0px 10px 0px 10px; background-color: #555555" align="center" cellspacing="0">
            <tr>
            <td align="center" valign="middle" bgcolor="#555555" class="style6" width="100px"><asp:Label ID="lbltotal" runat="server" Text="Total" Font-Size="10pt" Font-Bold="True" ForeColor="White"></asp:Label><br />
                            <asp:Label ID="lblMAS" runat="server" Text="Alam 4" Font-Size="10pt" Font-Bold="True" ForeColor="White"></asp:Label>
            </td>
            <td>
            <table border="0" cellpadding="0" cellspacing="0">
            <tr >
            <td width="100px" 
        
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;  padding-top: 3px; padding-bottom: 3px;" 
                    class="style9">Coal Getting</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style11">DAILY</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style11">MTD</td>
            <td width="90px" 
        
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF; " 
                    class="style11">YTD</td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Target</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label1" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label2" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label3" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_CP")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Actual</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label4" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label5" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label6" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_CP")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Percentage</td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label32" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label33" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label34" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
        
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;  padding-top: 3px; padding-bottom: 3px;" 
                    class="style9">OB Removal</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style11">DAILY</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style11">MTD</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF; " 
                    class="style11">YTD</td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Target</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblPlan_Daily" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblPlan_Monthly" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblPlan_Yearly" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_OB")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Actual</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblDaily_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblMTD_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblYTD_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_OB")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Percentage</td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label35" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label36" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label37" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">SR</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label85" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRD")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label86" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRM")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label87" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRY")) %>'>
                            </asp:Label></td>
            </tr>
            </table>
            </td>
            </tr>

            </table>
            </ItemTemplate>
                <SeparatorStyle BackColor="WhiteSmoke" />
                <SeparatorTemplate>
        
                </SeparatorTemplate>
            </asp:DataList>
                <br />
                <asp:DataList ID="DLProduction5" runat="server" BackColor="White" 
                                    BorderColor="#555555" Width="520px" 
                    BorderStyle="None" BorderWidth="2px" CellPadding="5" DataSourceID="DSProduction5" 
                    ForeColor="Black" GridLines="Vertical" 
                                    style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt">
                <AlternatingItemStyle BackColor="#555555" />
                <FooterStyle BackColor="#CCCC99" />
                <HeaderStyle BackColor="WhiteSmoke" Font-Bold="True" ForeColor="Red" Font-Size="12pt" 
                                        Height="20px" />
                <HeaderTemplate>
                    <asp:Label ID="Label4" runat="server" Font-Size="12pt">Production Alam 5</asp:Label>
                </HeaderTemplate>
                <ItemStyle BackColor="#555555" Font-Size="9pt" />
                <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <ItemTemplate>
            <table style="padding: 0px 10px 0px 10px; background-color: #555555" align="center" cellspacing="0">
            <tr>
            <td align="center" valign="middle" width="100px" bgcolor="#555555">
                <span class="style1">Contractor:</span><br />
                            <asp:Label ID="CategoryIDLabel" runat="server" 
                                Text='<%# Eval("Contractor_Id") %>' Font-Size="12pt" 
                    Font-Bold="True" ForeColor="White"></asp:Label>
            </td>
            <td>
            <table border="0" cellpadding="0" cellspacing="0">
            <tr style="margin: 0px; padding: 0px;">
            <td width="100px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;  padding-top: 3px; padding-bottom: 3px;">Coal Getting</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style2">Daily</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style2">MTD</td>
            <td width="90px" 
        
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF; " 
                    class="style2">YTD</td>
            </tr>
            <tr>
            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" bgcolor="White">Target</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label1" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label2" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" style="padding-right: 5px" bgcolor="White"><asp:Label ID="Label3" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_CP")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" bgcolor="White">Actual</td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label85" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label5" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" style="padding-right: 5px" bgcolor="White"><asp:Label ID="Label6" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_CP")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" bgcolor="White">Percentage</td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label261" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label271" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" style="padding-right: 5px" bgcolor="White">
                <asp:Label ID="Label281" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;  padding-top: 3px; padding-bottom: 3px;">OB Removal</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style2">Daily</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style2">MTD</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF; " 
                    class="style2">YTD</td>
            </tr>
            <tr>
            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" bgcolor="White">Target</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblPlan_Daily" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblPlan_Monthly" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" style="padding-right: 5px" bgcolor="White"><asp:Label ID="lblPlan_Yearly" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_OB")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" bgcolor="White">Actual</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblDaily_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblMTD_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" style="padding-right: 5px" bgcolor="White"><asp:Label ID="lblYTD_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_OB")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" bgcolor="White">Percentage</td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label291" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label301" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" style="padding-right: 5px" bgcolor="White">
                <asp:Label ID="Label311" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            </tr>
            <tr>
            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" bgcolor="White">SR</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label82" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRD")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label83" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRM")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" style="padding-right: 5px" bgcolor="White"><asp:Label ID="Label84" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRY")) %>'>
                            </asp:Label></td>
            </tr>
            </table>
            </td>
            </tr>

            </table>
            </ItemTemplate>
                <SeparatorStyle BackColor="WhiteSmoke" />
            </asp:DataList>
                <br />
                <asp:DataList ID="DLProductionTotal5" runat="server" BackColor="White" 
                                    BorderColor="#DEDFDE" Width="520px" 
                    BorderStyle="None" CellPadding="5" DataSourceID="DSProductionTotal5" 
                    ForeColor="Black" GridLines="Vertical" 
                                    style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt" 
                                    ShowFooter="False">
                <AlternatingItemStyle BackColor="#555555" />
                <FooterStyle BackColor="#E3DFDF" />
                <FooterTemplate></FooterTemplate>
                <HeaderStyle BackColor="#E3DFDF" Font-Bold="True" ForeColor="Red" Font-Size="12pt" Height="0px" />
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemStyle BackColor="#555555" Font-Size="9pt" />
                <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <ItemTemplate>
            <table style="padding: 0px 10px 0px 10px; background-color: #555555" align="center" cellspacing="0">
            <tr>
            <td align="center" valign="middle" bgcolor="#555555" class="style6"><asp:Label ID="lbltotal" runat="server" Text="Total" Font-Size="10pt" Font-Bold="True" ForeColor="White"></asp:Label><br />
                            <asp:Label ID="lblMAS" runat="server" Text="Alam 5" Font-Size="12pt" Font-Bold="True" ForeColor="White"></asp:Label>
            </td>
            <td>
            <table border="0" cellpadding="0" cellspacing="0">
            <tr style="margin: 0px; padding: 0px;">
            <td width="100px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;  padding-top: 3px; padding-bottom: 3px;">Coal Getting</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style1">Daily</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style1">MTD</td>
            <td width="90px" 
        
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF; " 
                    class="style1">YTD</td>
            </tr>
            <tr>
            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" bgcolor="White">Target</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label1" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label2" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" style="padding-right: 5px" bgcolor="White"><asp:Label ID="Label3" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_CP")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" bgcolor="White">Actual</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label4" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label5" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" style="padding-right: 5px" bgcolor="White"><asp:Label ID="Label6" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_CP")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" bgcolor="White">Percentage</td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label321" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label331" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" style="padding-right: 5px" bgcolor="White">
                <asp:Label ID="Label341" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;  padding-top: 3px; padding-bottom: 3px;">OB Removal</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style1">Daily</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                    class="style1">MTD</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #555555; color: #FFFFFF; " 
                    class="style1">YTD</td>
            </tr>
            <tr>
            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" bgcolor="White">Target</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblPlan_Daily" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblPlan_Monthly" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" style="padding-right: 5px" bgcolor="White"><asp:Label ID="lblPlan_Yearly" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_OB")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" bgcolor="White">Actual</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblDaily_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblMTD_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" style="padding-right: 5px" bgcolor="White"><asp:Label ID="lblYTD_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_OB")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" bgcolor="White">Percentage</td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label351" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label361" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" style="padding-right: 5px" bgcolor="White">
                <asp:Label ID="Label371" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            </tr>
            <tr>
            <td width="100px" style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" bgcolor="White">SR</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label85" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRD")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label86" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRM")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" style="padding-right: 5px" bgcolor="White"><asp:Label ID="Label87" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRY")) %>'>
                            </asp:Label></td>
            </tr>
            </table>
            </td>
            </tr>

            </table>
            </ItemTemplate>
                <SeparatorStyle BackColor="WhiteSmoke" />
                <SeparatorTemplate>
        
                </SeparatorTemplate>
            </asp:DataList>
                <br />
                <asp:DataList ID="DLProductionTotalAll" runat="server" BackColor="White" 
                                    BorderColor="#DEDFDE" Width="520px" 
                    BorderStyle="None" CellPadding="5" DataSourceID="DSProductionTotalAll" 
                    ForeColor="Black" GridLines="Vertical" 
                                    style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt" 
                                    ShowFooter="False">
                <AlternatingItemStyle BackColor="Black" />
                <FooterStyle BackColor="#E3DFDF" />
                <FooterTemplate></FooterTemplate>
                <HeaderStyle BackColor="#E3DFDF" Font-Bold="True" ForeColor="Red" Font-Size="12pt" 
                                        Height="0px" />
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemStyle BackColor="Black" Font-Size="8pt" />
                <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <ItemTemplate>
            <table style="padding: 0px 10px 0px 10px; background-color: #000000" align="center" cellspacing="0">
            <tr>
            <td align="center" valign="middle" width="100px" bgcolor="Black">
                <asp:Label ID="lbltotal" runat="server" Text="Total" Font-Size="Medium" 
                    Font-Bold="True" ForeColor="White" style="font-size: small"></asp:Label><br />
                            <asp:Label ID="lblMAS" runat="server" Text="MAS PRODUCTION" 
                    Font-Size="X-Large" Font-Bold="True" ForeColor="White" 
                    style="font-size: small"></asp:Label>
            </td>
            <td>
            <table border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td width="100px" 
        
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; padding-top: 3px; padding-bottom: 3px; " 
                    class="style9">Coal Getting</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                    class="style11">DAILY</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                    class="style11">MTD</td>
            <td width="90px" 
        
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " 
                    class="style11">YTD</td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Target</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label1" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label2" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label3" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_CP")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Actual</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label4" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label5" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_CP")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label6" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_CP")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Percentage</td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label32" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label33" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label34" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
        
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; padding-top: 3px; padding-bottom: 3px; " 
                    class="style9">OB Removal</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                    class="style11">DAILY</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                    class="style11">MTD</td>
            <td width="90px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " 
                    class="style11">YTD</td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Target</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblPlan_Daily" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblPlan_Monthly" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblPlan_Yearly" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_OB")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Actual</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblDaily_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblMTD_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_OB")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="lblYTD_Actual" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_OB")) %>'>
                            </asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">Percentage</td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label35" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label36" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            <td width="90px" align="right" bgcolor="White">
                <asp:Label ID="Label37" runat="server" 
                                Font-Bold="True" Width="100%"></asp:Label></td>
            </tr>
            <tr>
            <td width="100px" 
                    style="font-weight: bold; padding-left: 5px;  padding-top: 3px; padding-bottom: 3px;" 
                    bgcolor="White" class="style9">SR</td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label85" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRD")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label86" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRM")) %>'>
                            </asp:Label></td>
            <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label87" runat="server" 
                                Text='<%#string.Format("{0:n2}",Eval("SRY")) %>'>
                            </asp:Label></td>
            </tr>
            </table>
            </td>
            </tr>

            </table>
            </ItemTemplate>
                <SeparatorStyle BackColor="WhiteSmoke" Height="10px" />
                <SeparatorTemplate>
        
                </SeparatorTemplate>
            </asp:DataList>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>


    <table border="0" cellpadding="5px" cellspacing="0px" style="height:auto; text-align: center; width: 21cm;">
        <tr>
        <td valign="top">
        <asp:DataList ID="DLCrushing" runat="server" BackColor="White" 
                            BorderColor="#DEDFDE" Width="520px" 
            BorderStyle="None" BorderWidth="2px" CellPadding="5" DataSourceID="DSCrushing" 
            ForeColor="Black" GridLines="Vertical" 
                            style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt" 
                            ShowFooter="False">
        <AlternatingItemStyle BackColor="#555555" />
        <FooterStyle BackColor="#CCCC99" />
        <HeaderStyle BackColor="WhiteSmoke" Font-Bold="True" ForeColor="Red" Font-Size="12pt" 
                                Height="10px" />
        <HeaderTemplate>
            <asp:Label ID="Label7" runat="server" Font-Size="12pt">Crushing</asp:Label>
        </HeaderTemplate>
        <ItemStyle BackColor="#555555" Font-Size="9pt" />
        <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
        <ItemTemplate>
        <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px 10px 0px 5px; background-color: #555555" align="center">
        <tr>
        <td width="90px" rowspan="2"
        
            style="font-weight: bold; background-color: #555555; color: #FFFFFF; text-align: left;">
            <asp:Label ID="lblPlantNo" runat="server" 
                        Text='<%# Eval("plantno") %>'
            Font-Bold="True" ForeColor="White" Font-Size="10pt"></asp:Label></td>
        <td width="90px" align="center" colspan="3"
            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
            class="style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DAILY</td>
        <td width="90px" align="right" 
            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
            class="style7">MTD</td>
        <td width="90px" align="right" 
            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
            class="style7">YTD</td>
        </tr>

        <tr >
        <td width="90px" align="right"
        
            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
            class="style7">Shift 1</td>
        <td width="90px" align="right"
        
            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
            class="style7">Shift 2</td>
        <td width="90px" align="right"
        
            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
            class="style7">Total</td>
        <td width="90px" align="right"
        
            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
            class="style7">Total</td>
        <td width="90px" align="right"
        
            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
            class="style7">Total</td>
        </tr>
        <tr>
        <td width="90px" 
            style="font-weight: bold; padding-left: 5px; padding-top: 3px;  padding-bottom: 3px; color: #FFFFFF; text-align: left; background-color: #555555;" 
            class="style7">Target</td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label1" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_S1")) %>'>
                    </asp:Label></td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label8" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_S2")) %>'>
                    </asp:Label></td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label13" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_S1") + Eval("Plan_Daily_S2")) %>' Font-Bold="True">
                    </asp:Label></td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label72" runat="server"
                        Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_S1") + Eval("Plan_Monthly_S2")) %>'>
                    </asp:Label></td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label74" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_S1") + Eval("Plan_Yearly_S2")) %>'>
                    </asp:Label></td>
        </tr>
        <tr>
        <td width="90px" 
            style="font-weight: bold; padding-left: 5px; padding-top: 3px;  padding-bottom: 3px; color: #FFFFFF; text-align: left; background-color: #555555;" 
            class="style7">Actual</td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label2" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_S1")) %>'>
                    </asp:Label></td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label9" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_S2")) %>'>
                    </asp:Label></td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label14" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Qty_Daily_S1") + Eval("Qty_Daily_S2")) %>' Font-Bold="True">
                    </asp:Label></td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label3" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly_S1") + Eval("Qty_Monthly_S2")) %>'>
                    </asp:Label></td>
        <td width="90px" align="right"bgcolor="White"><asp:Label ID="Label11" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly_S1") + Eval("Qty_Yearly_S2")) %>'>
                    </asp:Label></td>


        </tr>
        <tr>
        <td width="90px" 
            style="font-weight: bold; padding-left: 5px; padding-top: 3px;  padding-bottom: 3px; color: #FFFFFF; text-align: left; background-color: #555555;" 
            class="style7">Percentage</td>
        <td width="90px" align="right" bgcolor="White">
        <asp:Label ID="Label44" runat="server" 
                        Font-Bold="True" Width="100%"></asp:Label></td>
        <td width="90px" align="right" bgcolor="White">
        <asp:Label ID="Label45" runat="server" 
                        Font-Bold="True" Width="100%"></asp:Label></td>
        <td width="90px" align="right" bgcolor="White">
        <asp:Label ID="Label46" runat="server" 
                        Font-Bold="True" Width="100%"></asp:Label></td>
        <td width="90px" align="right" bgcolor="White">
        <asp:Label ID="Label10" runat="server" 
                        Font-Bold="True" Width="100%"></asp:Label></td>
        <td width="90px" align="right" bgcolor="White">
        <asp:Label ID="Label12" runat="server"
                        Font-Bold="True" Width="100%"></asp:Label></td>
        </tr>
        <tr>
        <td width="90px" 
            style="font-weight: bold; padding-left: 5px; padding-top: 3px;  padding-bottom: 3px; color: #FFFFFF; text-align: left; background-color: #555555;" 
            class="style7">WH</td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label88" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("wh_daily_s1")) %>'>
                    </asp:Label></td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label89" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("wh_daily_s2")) %>'>
                    </asp:Label></td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label90" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("wh_daily_s1") + Eval("wh_daily_s2")) %>' Font-Bold="True">
                    </asp:Label></td>
        <td width="90px" align="right"  bgcolor="White"><asp:Label ID="Label91" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("wh_monthly_s1") + Eval("wh_monthly_s2")) %>'>
                    </asp:Label></td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label92" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("wh_yearly_s1") + Eval("wh_yearly_s2")) %>'>
                    </asp:Label></td>
        </tr>
        </tr>
        <tr>
        <td width="90px" 
            style="font-weight: bold; padding-left: 5px; padding-top: 3px;  padding-bottom: 3px; color: #FFFFFF; text-align: left; background-color: #555555;" 
            class="style7">Productivity</td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label93" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("prod_daily_s1")) %>'>
                    </asp:Label></td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label94" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("prod_daily_s2")) %>'>
                    </asp:Label></td>
        <td width="90px" align="right" bgcolor="White"><asp:Label ID="Label95" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("prod_daily_total")) %>' Font-Bold="True">
                    </asp:Label></td>
        <td width="90px" align="right"  bgcolor="White"><asp:Label ID="Label96" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("prod_monthly_total")) %>'>
                    </asp:Label></td>
        <td width="90px" align="right"  bgcolor="White"><asp:Label ID="Label97" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("prod_yearly_total")) %>'>
                    </asp:Label></td>
        </tr>
        </table>
        </ItemTemplate>
        <SeparatorStyle BackColor="WhiteSmoke" />
        <SeparatorTemplate>
        
        </SeparatorTemplate>
        </asp:DataList>
        <asp:DataList ID="DLCrushingTotal" runat="server" BackColor="White" 
                            BorderColor="#DEDFDE" Width="520px" 
            BorderStyle="None" BorderWidth="2px" CellPadding="5" DataSourceID="DSCrushingTotal" 
            ForeColor="Black" GridLines="Vertical" 
                            style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt" 
                            ShowFooter="False">
        <HeaderStyle BackColor="#E3DFDF" Font-Bold="True" ForeColor="#E3DFDF" Font-Size="12pt" 
                                Height="0px" />
        <HeaderTemplate>
        
        </HeaderTemplate>
        <AlternatingItemStyle BackColor="#333333" />
        <FooterStyle BackColor="#E3DFDF" Height="0px" />
        <FooterTemplate>
    
        </FooterTemplate>
        <ItemStyle BackColor="#333333" Font-Size="8pt" />
        <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
        <ItemTemplate>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 0px 10px 0px 10px; background-color: #333333" align="center">
        <tr>
        <td width="130px"
        
        
            style="background-color: #333333; " 
            bgcolor="#333333">
            <asp:Label ID="lblPlantNo" runat="server" 
                        Text="Total Crushing"
            Font-Bold="True" ForeColor="White" Font-Size="10pt" Width="150px" 
                style="text-align: left" ></asp:Label></td>
        <td width="130px"
        
            style="font-weight: bold; background-color: #333333; color: #FFFFFF; " 
            class="style13" bgcolor="#333333">DAILY</td>
        <td width="130px" 
            style="font-weight: bold; background-color: #333333; color: #FFFFFF;" 
            class="style13" bgcolor="#333333">MTD</td>
        <td width="130px" 
        
            style="font-weight: bold; background-color: #333333; color: #FFFFFF; " 
            class="style13" bgcolor="#333333">YTD</td>
        </tr>


        <tr>
        <td width="130px" 
        
            style="font-weight: bold; background-color: #333333; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; text-align: left;" 
            bgcolor="#333333"><b>Target</b></td>
        <td width="130px" align="right" bgcolor="White"><asp:Label ID="Label1" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Plan_Daily")) %>'>
                    </asp:Label></td>
        <td width="130px" align="right" bgcolor="White"><asp:Label ID="Label15" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly")) %>'>
                    </asp:Label></td>
        <td width="130px" align="right" bgcolor="White"><asp:Label ID="Label16" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly")) %>'>
                    </asp:Label></td>
        </tr>

        <tr>
        <td width="130px" 
        
            style="font-weight: bold; background-color: #333333; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; text-align: left;" 
            bgcolor="#333333"><b>Actual</b></td>
        <td width="130px" align="right" bgcolor="White"><asp:Label ID="Label2" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Qty_Daily")) %>'>
                    </asp:Label></td>
        <td width="130px" align="right" bgcolor="White"><asp:Label ID="Label3" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly")) %>'>
                    </asp:Label></td>
        <td width="130px" align="right" bgcolor="White"><asp:Label ID="Label11" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly")) %>'>
                    </asp:Label></td>
        </tr>

        <tr>
        <td width="130px" 
        
            style="font-weight: bold; background-color: #333333; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; text-align: left;" 
            bgcolor="#333333"><b>Percentage</b></td>
        <td width="130px" align="right" bgcolor="White">
        <asp:Label ID="Label47" runat="server" 
                        Font-Bold="True" Width="100%"></asp:Label></td>
        <td width="130px" align="right" bgcolor="White">
        <asp:Label ID="Label73" runat="server" 
                        Font-Bold="True" Width="100%"></asp:Label></td>
        <td width="130px" align="right" bgcolor="White">
        <asp:Label ID="Label75" runat="server" 
                        Font-Bold="True" Width="100%"></asp:Label></td>
        </tr>

        <tr>
        <td width="130px" 
        
            style="font-weight: bold; background-color: #333333; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; text-align: left;" 
            bgcolor="#333333"><b>WH</b></td>
        <td width="130px" align="right" bgcolor="White"><asp:Label ID="Label98" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("wh_daily")) %>'>
                    </asp:Label></td>
        <td width="130px" align="right" bgcolor="White"><asp:Label ID="Label99" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("wh_monthly")) %>'>
                    </asp:Label></td>
        <td width="130px" align="right" bgcolor="White"><asp:Label ID="Label100" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("wh_yearly")) %>'>
                    </asp:Label></td>
        </tr>

        <tr>
        <td width="130px" 
        
            style="font-weight: bold; background-color: #333333; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; text-align: left;" 
            bgcolor="#333333"><b>Productivity</b></td>
        <td width="130px" align="right" bgcolor="White"><asp:Label ID="Label101" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("prod_daily")) %>'>
                    </asp:Label></td>
        <td width="130px" align="right" style="padding-right: 5px" bgcolor="White"><asp:Label ID="Label102" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("prod_monthly")) %>'>
                    </asp:Label></td>
        <td width="130px" align="right" style="padding-right: 5px" bgcolor="White"><asp:Label ID="Label103" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("prod_yearly")) %>'>
                    </asp:Label></td>
        </tr>
        </table>
        </ItemTemplate>
        <SeparatorStyle BackColor="WhiteSmoke" />
        <SeparatorTemplate>
        
        </SeparatorTemplate>
        </asp:DataList>
        </td>
        <td valign="top">
        <asp:DataList ID="DLLogistics" runat="server" BackColor="White" 
                            BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="2px" CellPadding="5" 
                            DataSourceID="DSHauling" ForeColor="Black" GridLines="Vertical" 
                            style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt" 
                            Width="520px" ShowFooter="False">
                            <AlternatingItemStyle BackColor="#555555" />
                            <FooterStyle BackColor="#F5F5F5" />
                            <HeaderStyle BackColor="#F5F5F5" Font-Bold="True" Font-Size="12pt" 
                                ForeColor="Red" Height="10px" />
                            <HeaderTemplate>
                                <asp:Label ID="Label7" runat="server" Font-Size="12pt">Hauling</asp:Label>
                            </HeaderTemplate>
                            <ItemStyle BackColor="#555555" Font-Size="9pt" />
                            <SelectedItemStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="White" />
                            <ItemTemplate>
                                <table border="0" cellpadding="0" cellspacing="0" 
                                    style="padding: 0px 10px 0px 10px; background-color: #555555">
                                    <tr>
                                        <td align="left" 
                                            style="font-weight: bold; background-color: #555555; color: #FFFFFF; " 
                                            width="150px" bgcolor="#CCCCCC">
                                            <asp:Label ID="Label1" runat="server" 
                                                Text='<%# string.Format("{0:n2}",Eval("Activity_data")) %>' 
                                                Font-Size="10pt" ForeColor="White"></asp:Label>
                                        </td>
                                        <td align="right" 
                                            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                                            width="123px" class="style7">
                                            DAILY</td>
                                        <td align="right" 
                                            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                                            width="123px" bgcolor="#E3DFDF" class="style7">
                                            MTD</td>
                                        <td align="right" 
                                            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
                                            width="123px" bgcolor="#E3DFDF" class="style7">
                                            YTD</td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; background-color: #555555; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; text-align: left;" 
                                            width="150px">
                                            Target</td>
                                        <td align="right" style="background-color: #FFFFFF" width="123px" 
                                            bgcolor="White">
                                            <asp:Label ID="lblTRKD" runat="server" 
                                                Text='<%#string.Format("{0:n2}",Eval("Plan_Daily_TRUS")) %>'>
                    </asp:Label>
                                        </td>
                                        <td align="right" style="background-color: #FFFFFF" width="123px" 
                                            bgcolor="White">
                                            <asp:Label ID="lblTRKM" runat="server" 
                                                Text='<%#string.Format("{0:n2}",Eval("Plan_Monthly_TRUS")) %>'>
                    </asp:Label>
                                        </td>
                                        <td align="right" style="background-color: #FFFFFF" width="123px" 
                                            bgcolor="White">
                                            <asp:Label ID="lblTRKY" runat="server" 
                                                Text='<%#string.Format("{0:n2}",Eval("Plan_Yearly_TRUS")) %>'>
                    </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; background-color: #555555; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; text-align: left;" 
                                            width="150px">
                                            Actual</td>
                                        <td align="right" style="background-color: #FFFFFF" width="123px" 
                                            bgcolor="White">
                                            <asp:Label ID="lblDaily" runat="server" 
                                                Text='<%#string.Format("{0:n2}",Eval("Qty_Daily")) %>'>
                    </asp:Label>
                                        </td>
                                        <td align="right" style="background-color: #FFFFFF" width="123px" 
                                            bgcolor="White">
                                            <asp:Label ID="lblMonthly" runat="server" 
                                                Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly")) %>'>
                    </asp:Label>
                                        </td>
                                        <td align="right" style="background-color: #FFFFFF" width="123px" 
                                            bgcolor="White">
                                            <asp:Label ID="lblYearly" runat="server" 
                                                Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly")) %>'>
                    </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; background-color: #555555; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; text-align: left;" 
                                            width="150px">
                                            Percentage</td>
                                        <td align="right" style="background-color: #FFFFFF" width="123px" 
                                            bgcolor="White">
                                            <asp:Label ID="Label38" runat="server" Font-Bold="True" Width="100%"></asp:Label>
                                        </td>
                                        <td align="right" style="background-color: #FFFFFF" width="123px" 
                                            bgcolor="White">
                                            <asp:Label ID="Label39" runat="server" Font-Bold="True" Width="100%"></asp:Label>
                                        </td>
                                        <td align="right" style="background-color: #FFFFFF" width="123px" 
                                            bgcolor="White">
                                            <asp:Label ID="Label40" runat="server" Font-Bold="True" Width="100%"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <SeparatorStyle BackColor="WhiteSmoke" />
                            <SeparatorTemplate>
                            </SeparatorTemplate>
                        </asp:DataList>
        <asp:DataList ID="DLHaulingTotal" runat="server" BackColor="White" 
                            BorderColor="#DEDFDE" Width="520px" 
            BorderStyle="None" BorderWidth="2px" CellPadding="5" DataSourceID="DSHaulingTotal" 
            ForeColor="Black" GridLines="Vertical" 
                            style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt" 
                            Visible="false" ShowFooter="False">
        <AlternatingItemStyle BackColor="#F5F5F5" />
        <FooterStyle BackColor="#F5F5F5" Height="10" />
        <HeaderStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="Red" Height="10" />
        <HeaderTemplate></HeaderTemplate>
        <ItemStyle BackColor="#F5F5F5" Font-Size="8pt" />
        <SelectedItemStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="White" />
        <ItemTemplate>
        <table border="0" cellpadding="3" cellspacing="0" style="padding: 0px 10px 0px 10px; background-color: #ABA4A4">
        <tr>
        <td width="130px" align="left" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF; ">
        <asp:Label ID="Label1" runat="server" 
                        Text='<%# Eval("Activity_Data") %>'></asp:Label></td>
        <td width="130px" align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;"><asp:Label ID="Label17" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Qty_Daily")) %>'>
                    </asp:Label></td>
        <td width="130px" align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;"><asp:Label ID="Label18" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly")) %>'>
                    </asp:Label></td>
        <td width="130px" align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;"><asp:Label ID="Label19" runat="server" 
                        Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly")) %>'>
                    </asp:Label></td>
        </tr>
        </table>
        </ItemTemplate>
        <SeparatorStyle BackColor="#F5F5F5" />
        <SeparatorTemplate>
        
        </SeparatorTemplate>
        </asp:DataList>
        </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
   </table>

    <div style="page-break-after: always"></div>

    <table border="0" cellpadding="5px" cellspacing="0px" style="height:auto; text-align: center; width: 21cm;">
        <tr>
        <td valign="top" class="style16">
                        <asp:DataList ID="DLSales" runat="server" BackColor="White" 
                            BorderColor="#DEDFDE" Width="520px" 
            BorderStyle="None" BorderWidth="2px" CellPadding="5" DataSourceID="DSSales" 
            ForeColor="Black" GridLines="Vertical" ShowFooter="False" 
            style="font-family: Arial, Helvetica, sans-serif">
        <AlternatingItemStyle BackColor="#555555" />
        <FooterStyle BackColor="#F5F5F5" />
        <HeaderStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="Red" Font-Size="12pt" Height="10px" />
        <HeaderTemplate>
            <%--asp:Label ID="Label7" runat="server" Font-Size="12pt">Barging Alam 1,2,3</asp:Label--%>
            <asp:Label ID="Label7" runat="server" Font-Size="12pt">GAR 4800</asp:Label>
        </HeaderTemplate>
        <ItemStyle BackColor="#555555" Font-Size="9pt" />
        <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
    <ItemTemplate>
    <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px 10px 0px 10px; background-color: #555555">
    <tr>
    <td width="130PX" align="left" 
        
            style="font-weight: bold; background-color: #555555; color: #FFFFFF; ">
        <asp:Label ID="lblBarge_Act" runat="server" 
                       Text='<%# string.Format("{0:n2}",Eval("Barge")) %>' 
            Font-Size="10pt" ForeColor="White"></asp:Label></td>
    <td width="130px" align="right" 
            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
            class="style7">&nbsp;</td>
    <td width="130px" align="right" 
            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
            class="style7">MTD</td>
    <td width="130px" align="right" 
            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
            class="style7">YTD</td>
    </tr>
    <tr>
    <td width="130PX" 
            style="font-weight: bold; background-color: #555555; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px;" 
            class="style10">Target</td>
    <td width="130px" align="right" style="background-color: #FFFFFF">
        <asp:Label ID="lblPB_Daily" runat="server" 
                       Text='<%# string.Format("{0:n2}",Eval("PLAN_DAILY")) %>' 
            Visible="False"></asp:Label></td>
    <td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label ID="lblPB_Monthly" runat="server" 
                       Text='<%#string.Format("{0:n2}",Eval("PLAN_MONTHLY")) %>'>
                    </asp:Label></td>
    <td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label ID="lblPY_Monthly" runat="server" 
                       Text='<%#string.Format("{0:n2}",Eval("PLAN_YEARLY")) %>'>
                    </asp:Label></td>
    </tr>
    <tr>
    <td width="130PX" 
            style="font-weight: bold; background-color: #555555; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px;" 
            class="style10">Actual</td>
    <td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label 
            ID="lblDaily" runat="server" 
                       Text='<%# string.Format("{0:n2}",Eval("QTY_DAILY")) %>' 
            Visible="False"></asp:Label></td>
    <td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label ID="lblMonthly" runat="server" 
                       Text='<%#string.Format("{0:n2}",Eval("QTY_MONTHLY")) %>'>
                    </asp:Label></td>
    <td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label ID="lblYearly" runat="server" 
                       Text='<%#string.Format("{0:n2}",Eval("QTY_YEARLY")) %>'>
                    </asp:Label></td>
    </tr>
    <tr>
    <td width="130PX" 
            style="font-weight: bold; background-color: #555555; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px;" 
            class="style10">Percentage</td>
    <td width="130px" align="right" style="background-color: #FFFFFF">
        <asp:Label ID="Label41" runat="server" 
                       Font-Bold="True" Visible="False"></asp:Label></td>
    <td width="130px" align="right" style="background-color: #FFFFFF">
        <asp:Label ID="Label42" runat="server" 
                       Font-Bold="True" Width="100%"></asp:Label></td>
    <td width="130px" align="right" style="background-color: #FFFFFF">
        <asp:Label ID="Label43" runat="server" 
                       Font-Bold="True" Width="100%"></asp:Label></td>
    </tr>
    </table>
    </ItemTemplate>
        <SeparatorStyle BackColor="WhiteSmoke" />
        <SeparatorTemplate>
        
        </SeparatorTemplate>
    </asp:DataList>
                        <asp:DataList ID="DLSales_Total" runat="server" BackColor="White" 
                            BorderColor="#DEDFDE" Width="520px" 
            BorderStyle="None" BorderWidth="2px" CellPadding="5" DataSourceID="DSSales_Total" 
            ForeColor="Black" GridLines="Vertical" ShowFooter="False" 
                style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt">
        <AlternatingItemStyle BackColor="#F5F5F5" />
        <FooterStyle BackColor="#F5F5F5" Height="0px" />
        <HeaderStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="Red" Height="0px" />
        <HeaderTemplate></HeaderTemplate>
        <ItemStyle BackColor="#F5F5F5" Font-Size="8pt" />
        <SelectedItemStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="White" />
    <ItemTemplate>
    <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px 10px 0px 10px; background-color: #000000">
    <tr class="style15">
    <td width="130px" align="left" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; ">
        TARGET</td>
    <td width="130px" align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;">
        <asp:Label ID="Label25" runat="server" 
            Text='<%# string.Format("{0:n2}",Eval("PLAN_DAILY")) %>' Visible="False"></asp:Label>
        </td>
    <td width="130px" align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;">
        <asp:Label ID="Label20" runat="server" 
            Text='<%# string.Format("{0:n2}",Eval("PLAN_MONTHLY")) %>'></asp:Label>
        </td>
    <td width="130px" align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;">
        <asp:Label ID="Label21" runat="server" 
            Text='<%# string.Format("{0:n2}",Eval("PLAN_YEARLY")) %>'></asp:Label>
        </td>
    </tr>
        <tr class="style15">
            <td align="left" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; " 
                width="130px">
                <asp:Label ID="Label1" runat="server" Text="ACTUAL"></asp:Label>
            </td>
            <td align="right" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                width="130px">
                <asp:Label ID="Label17" runat="server" 
                    Text='<%# string.Format("{0:n2}",Eval("QTY_DAILY")) %>' Visible="False"></asp:Label>
            </td>
            <td align="right" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                width="130px">
                <asp:Label ID="Label18" runat="server" 
                    Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly")) %>'>
                    </asp:Label>
            </td>
            <td align="right" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                width="130px">
                <asp:Label ID="Label19" runat="server" 
                    Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly")) %>'>
                    </asp:Label>
            </td>
        </tr>
        <tr class="style15">
            <td align="left" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; " 
                width="130px">
                PERCENTAGE</td>
            <td align="right" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                width="130px">
                &nbsp;</td>
            <td align="right" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                width="130px">
                <asp:Label ID="Label23" runat="server" 
                    Text='<%# string.Format("{0:n2}",Eval("Qty_Yearly")) %>' Width="100%"></asp:Label>
            </td>
            <td align="right" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                width="130px">
                <asp:Label ID="Label24" runat="server" 
                    Text='<%# string.Format("{0:n2}",Eval("Qty_Yearly")) %>' Width="100%"></asp:Label>
            </td>
        </tr>
    </table>
    </ItemTemplate>
        <SeparatorStyle BackColor="#F5F5F5" />
        <SeparatorTemplate>
        
        </SeparatorTemplate>
    </asp:DataList>
    
        </td>
        <td valign="top" class="style16">
                        &nbsp;</td>
        <td valign="top" class="style16">
                        <asp:DataList ID="DLSales4" runat="server" BackColor="White" 
                            BorderColor="#DEDFDE" Width="520px" 
            BorderStyle="None" BorderWidth="2px" CellPadding="5" DataSourceID="DSSales4" 
            ForeColor="Black" GridLines="Vertical" 
                            style="font-size: 8pt; font-family: Arial, Helvetica, sans-serif" 
                            ShowFooter="False">
        <AlternatingItemStyle BackColor="#555555" />
        <FooterStyle BackColor="#F5F5F5" />
        <HeaderStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="Red" Font-Size="12pt" Height="10px" />
        <HeaderTemplate>
            <%--asp:Label ID="Label7" runat="server" Font-Size="12pt">Barging Alam 4</asp:Label--%>
            <asp:Label ID="Label7" runat="server" Font-Size="12pt">GAR 5300</asp:Label>
        </HeaderTemplate>
        <ItemStyle BackColor="#555555" Font-Size="9pt" />
        <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
    <ItemTemplate>
    <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px 10px 0px 10px; background-color: #555555">
    <tr>
    <td width="130px" align="left" 
        
            style="font-weight: bold; background-color: #555555; color: #FFFFFF; ">
        <asp:Label ID="lblBarge_Act" runat="server" 
                       Text='<%# string.Format("{0:n2}",Eval("Barge")) %>' 
            Font-Size="10pt" ForeColor="White"></asp:Label></td>
    <td width="130px" align="right" 
            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
            class="style7">&nbsp;</td>
    <td width="130px" align="right" 
            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
            class="style7">MTD</td>
    <td width="130px" align="right" 
            style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
            class="style7">YTD</td>
    </tr>
    <tr>
    <td width="90px" 
            style="font-weight: bold; background-color: #555555; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px;" 
            class="style10">Target</td>
    <td width="130px" align="right" style="background-color: #FFFFFF">
        <asp:Label ID="lblPB_Daily" runat="server" 
                       Text='<%# string.Format("{0:n2}",Eval("PLAN_DAILY")) %>' 
            Visible="False"></asp:Label></td>
    <td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label ID="lblPB_Monthly" runat="server" 
                       Text='<%#string.Format("{0:n2}",Eval("PLAN_MONTHLY")) %>'>
                    </asp:Label></td>
    <td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label ID="lblPY_Monthly" runat="server" 
                       Text='<%#string.Format("{0:n2}",Eval("PLAN_YEARLY")) %>'>
                    </asp:Label></td>
    </tr>
    <tr>
    <td width="90px" 
            style="font-weight: bold; background-color: #555555; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px;" 
            class="style10">Actual</td>
    <td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label 
            ID="lblDaily" runat="server" 
                       Text='<%# string.Format("{0:n2}",Eval("QTY_DAILY")) %>' 
            Visible="False"></asp:Label></td>
    <td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label ID="lblMonthly" runat="server" 
                       Text='<%#string.Format("{0:n2}",Eval("QTY_MONTHLY")) %>'>
                    </asp:Label></td>
    <td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label ID="lblYearly" runat="server" 
                       Text='<%#string.Format("{0:n2}",Eval("QTY_YEARLY")) %>'>
                    </asp:Label></td>
    </tr>
    <tr>
    <td width="90px" 
            style="font-weight: bold; background-color: #555555; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px;" 
            class="style10">Percentage</td>
    <td width="130px" align="right" style="background-color: #FFFFFF">
        <asp:Label ID="Label41" runat="server" 
                       Font-Bold="True" Visible="False"></asp:Label></td>
    <td width="130px" align="right" style="background-color: #FFFFFF">
        <asp:Label ID="Label42" runat="server" 
                       Font-Bold="True" Width="100%"></asp:Label></td>
    <td width="130px" align="right" style="background-color: #FFFFFF">
        <asp:Label ID="Label43" runat="server" 
                       Font-Bold="True" Width="100%"></asp:Label></td>
    </tr>
    </table>
    </ItemTemplate>
        <SeparatorStyle BackColor="#E3DFDF" />
        <SeparatorTemplate>
        
        </SeparatorTemplate>
    </asp:DataList>
                        <asp:DataList ID="DLSales_Total4" runat="server" BackColor="White" 
                            BorderColor="#DEDFDE" Width="520px" 
            BorderStyle="None" BorderWidth="2px" CellPadding="5" DataSourceID="DSSales_Total4" 
            ForeColor="Black" GridLines="Vertical" 
                            style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt" 
                            ShowFooter="False">
        <AlternatingItemStyle BackColor="#F5F5F5" />
        <FooterStyle BackColor="#F5F5F5" Height="0px" />
        <HeaderStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="Red" Height="0px" />
        <HeaderTemplate></HeaderTemplate>
        <ItemStyle BackColor="#F5F5F5" Font-Size="9pt" />
        <SelectedItemStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="White" />
    <ItemTemplate>
    <table border="0" cellpadding="0" cellspacing="0" style="padding: 0px 10px 0px 10px; background-color: #000000">
    <tr>
    <td width="130px" align="left" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; ">
        TARGET</td>
    <td width="130px" align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;">
        <asp:Label ID="Label24" runat="server" 
            Text='<%# string.Format("{0:n2}",Eval("PLAN_DAILY")) %>' Visible="False"></asp:Label>
        </td>
    <td width="130px" align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;">
        <asp:Label ID="Label20" runat="server" 
            Text='<%# string.Format("{0:n2}",Eval("PLAN_MONTHLY")) %>'></asp:Label>
        </td>
    <td width="130px" align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;">
        <asp:Label ID="Label21" runat="server" 
            Text='<%# string.Format("{0:n2}",Eval("PLAN_YEARLY")) %>'></asp:Label>
        </td>
    </tr>
        <tr>
            <td align="left" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; " 
                width="130px">
                <asp:Label ID="Label1" runat="server" Text="ACTUAL"></asp:Label>
            </td>
            <td align="right" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                width="130px">
                <asp:Label ID="Label17" runat="server" 
                    Text='<%# string.Format("{0:n2}",Eval("QTY_DAILY")) %>' Visible="False"></asp:Label>
            </td>
            <td align="right" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                width="130px">
                <asp:Label ID="Label18" runat="server" 
                    Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly")) %>'>
                    </asp:Label>
            </td>
            <td align="right" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                width="130px">
                <asp:Label ID="Label19" runat="server" 
                    Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly")) %>'>
                    </asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; " 
                width="130px">
                PERCENTAGE</td>
            <td align="right" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                width="130px">
                &nbsp;</td>
            <td align="right" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                width="130px">
                <asp:Label ID="Label22" runat="server" 
                    Text='<%# string.Format("{0:n2}",Eval("Qty_Monthly")) %>' Width="100%"></asp:Label>
            </td>
            <td align="right" 
                style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                width="130px">
                <asp:Label ID="Label23" runat="server" 
                    Text='<%# string.Format("{0:n2}",Eval("Qty_Monthly")) %>' Width="100%"></asp:Label>
            </td>
        </tr>
    </table>
    </ItemTemplate>
        <SeparatorStyle BackColor="#F5F5F5" Height="10px" />
        <SeparatorTemplate>
        
        </SeparatorTemplate>
    </asp:DataList>
    
        </td>
        </tr>
        <tr>
            <td class="style16">&nbsp;</td>
            <td class="style16">&nbsp;</td>
            <td class="style16">&nbsp;</td>
        </tr>
        <tr>
    <td class="style16">
                    <asp:DataList ID="DLSales5" runat="server" BackColor="White" 
                        BorderColor="#DEDFDE" Width="520px" 
        BorderStyle="None" BorderWidth="2px" CellPadding="5" DataSourceID="DSSales5" 
        ForeColor="Black" GridLines="Vertical" 
                        style="font-size: 8pt; font-family: Arial, Helvetica, sans-serif" 
                        ShowFooter="False">
    <AlternatingItemStyle BackColor="#555555" />
    <FooterStyle BackColor="#F5F5F5" />
    <HeaderStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="Red" Font-Size="12pt" Height="10px" />
    <HeaderTemplate>
        <%--asp:Label ID="Label7" runat="server" Font-Size="12pt">Barging Alam 5</asp:Label--%>
        <asp:Label ID="Label7" runat="server" Font-Size="12pt">GAR 4100</asp:Label>
    </HeaderTemplate>
    <ItemStyle BackColor="#555555" Font-Size="9pt" />
    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
<ItemTemplate>
<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px 10px 0px 10px; background-color: #555555">
<tr>
<td width="130px" align="left" 
        
        style="font-weight: bold; background-color: #555555; color: #FFFFFF; ">
    <asp:Label ID="lblBarge_Act" runat="server" 
                   Text='<%# string.Format("{0:n2}",Eval("Barge")) %>' 
        Font-Size="10pt" ForeColor="White"></asp:Label></td>
<td width="130px" align="right" 
        style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
        class="style7">&nbsp;</td>
<td width="130px" align="right" 
        style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
        class="style7">MTD</td>
<td width="130px" align="right" 
        style="font-weight: bold; background-color: #555555; color: #FFFFFF;" 
        class="style7">YTD</td>
</tr>
<tr>
<td width="90px" 
        style="font-weight: bold; background-color: #555555; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px;" 
        class="style10">Target</td>
<td width="130px" align="right" style="background-color: #FFFFFF">
    <asp:Label ID="lblPB_Daily" runat="server" 
                   Text='<%# string.Format("{0:n2}",Eval("PLAN_DAILY")) %>' 
        Visible="False"></asp:Label></td>
<td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label ID="lblPB_Monthly" runat="server" 
                   Text='<%#string.Format("{0:n2}",Eval("PLAN_MONTHLY")) %>'>
                </asp:Label></td>
<td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label ID="lblPY_Monthly" runat="server" 
                   Text='<%#string.Format("{0:n2}",Eval("PLAN_YEARLY")) %>'>
                </asp:Label></td>
</tr>
<tr>
<td width="90px" 
        style="font-weight: bold; background-color: #555555; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px;" 
        class="style10">Actual</td>
<td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label 
        ID="lblDaily" runat="server" 
                   Text='<%# string.Format("{0:n2}",Eval("QTY_DAILY")) %>' 
        Visible="False"></asp:Label></td>
<td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label ID="lblMonthly" runat="server" 
                   Text='<%#string.Format("{0:n2}",Eval("QTY_MONTHLY")) %>'>
                </asp:Label></td>
<td width="130px" align="right" style="background-color: #FFFFFF"><asp:Label ID="lblYearly" runat="server" 
                   Text='<%#string.Format("{0:n2}",Eval("QTY_YEARLY")) %>'>
                </asp:Label></td>
</tr>
<tr>
<td width="90px" 
        style="font-weight: bold; background-color: #555555; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px;" 
        class="style10">Percentage</td>
<td width="130px" align="right" style="background-color: #FFFFFF">
    <asp:Label ID="Label41" runat="server" 
                   Font-Bold="True" Visible="False"></asp:Label></td>
<td width="130px" align="right" style="background-color: #FFFFFF">
    <asp:Label ID="Label42" runat="server" 
                   Font-Bold="True" Width="100%"></asp:Label></td>
<td width="130px" align="right" style="background-color: #FFFFFF">
    <asp:Label ID="Label43" runat="server" 
                   Font-Bold="True" Width="100%"></asp:Label></td>
</tr>
</table>
</ItemTemplate>
    <SeparatorStyle BackColor="#E3DFDF" />
    <SeparatorTemplate>
        
    </SeparatorTemplate>
</asp:DataList>
                    <asp:DataList ID="DLSales_Total5" runat="server" BackColor="White" 
                        BorderColor="#DEDFDE" Width="520px" 
        BorderStyle="None" BorderWidth="2px" CellPadding="5" DataSourceID="DSSales_Total5" 
        ForeColor="Black" GridLines="Vertical" 
                        style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt" 
                        ShowFooter="False">
    <AlternatingItemStyle BackColor="#F5F5F5" />
    <FooterStyle BackColor="#F5F5F5" Height="0px" />
    <HeaderStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="Red" Height="0px" />
    <HeaderTemplate></HeaderTemplate>
    <ItemStyle BackColor="#F5F5F5" Font-Size="9pt" />
    <SelectedItemStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="White" />
<ItemTemplate>
<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px 10px 0px 10px; background-color: #000000">
<tr>
<td width="130px" align="left" 
        style="font-weight: bold; background-color: #000000; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; ">
    TARGET</td>
<td width="130px" align="right" 
        style="font-weight: bold; background-color: #000000; color: #FFFFFF;">
    <asp:Label ID="Label24" runat="server" 
        Text='<%# string.Format("{0:n2}",Eval("PLAN_DAILY")) %>' Visible="False"></asp:Label>
    </td>
<td width="130px" align="right" 
        style="font-weight: bold; background-color: #000000; color: #FFFFFF;">
    <asp:Label ID="Label20" runat="server" 
        Text='<%# string.Format("{0:n2}",Eval("PLAN_MONTHLY")) %>'></asp:Label>
    </td>
<td width="130px" align="right" 
        style="font-weight: bold; background-color: #000000; color: #FFFFFF;">
    <asp:Label ID="Label21" runat="server" 
        Text='<%# string.Format("{0:n2}",Eval("PLAN_YEARLY")) %>'></asp:Label>
    </td>
</tr>
    <tr>
        <td align="left" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; " 
            width="130px">
            <asp:Label ID="Label1" runat="server" Text="ACTUAL"></asp:Label>
        </td>
        <td align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
            width="130px">
            <asp:Label ID="Label17" runat="server" 
                Text='<%# string.Format("{0:n2}",Eval("QTY_DAILY")) %>' Visible="False"></asp:Label>
        </td>
        <td align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
            width="130px">
            <asp:Label ID="Label18" runat="server" 
                Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly")) %>'>
                </asp:Label>
        </td>
        <td align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
            width="130px">
            <asp:Label ID="Label19" runat="server" 
                Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly")) %>'>
                </asp:Label>
        </td>
    </tr>
    <tr>
        <td align="left" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF; padding-top: 3px;  padding-bottom: 3px; " 
            width="130px">
            PERCENTAGE</td>
        <td align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
            width="130px">
            &nbsp;</td>
        <td align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
            width="130px">
            <asp:Label ID="Label22" runat="server" 
                Text='<%# string.Format("{0:n2}",Eval("Qty_Monthly")) %>' Width="100%"></asp:Label>
        </td>
        <td align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
            width="130px">
            <asp:Label ID="Label23" runat="server" 
                Text='<%# string.Format("{0:n2}",Eval("Qty_Monthly")) %>' Width="100%"></asp:Label>
        </td>
    </tr>
</table>
</ItemTemplate>
    <SeparatorStyle BackColor="#F5F5F5" Height="10px" />
    <SeparatorTemplate>
        
    </SeparatorTemplate>
</asp:DataList>
    
        </td><td class="style16">
                    &nbsp;</td><td class="style16">
        <asp:DataList ID="DLSalesAll" runat="server" BackColor="White" 
                        BorderColor="#DEDFDE" Width="520px" 
        BorderStyle="None" BorderWidth="2px" CellPadding="5" DataSourceID="DSSalesAll" 
        ForeColor="Black" GridLines="Vertical" 
                        style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt" 
                    ShowFooter="False">
    <AlternatingItemStyle BackColor="Black" />
    <FooterStyle BackColor="#F5F5F5" />
    <HeaderStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="Red" Font-Size="12pt" 
                        Height="10px" />
    <HeaderTemplate>
        <asp:Label ID="Label7" runat="server" Font-Size="12pt">Total Barging</asp:Label>
    </HeaderTemplate>
    <ItemStyle BackColor="Black" Font-Size="8pt" />
    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
<ItemTemplate>
<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px 10px 0px 10px; background-color: #000000">
<tr>
<td width="130px" align="left" 
        
        style="font-weight: bold; background-color: #000000; color: #FFFFFF; ">
    <asp:Label ID="lblBarge_Act" runat="server" 
                   Text='<%# string.Format("{0:n2}",Eval("Barge")) %>' 
        Font-Size="10pt" ForeColor="White"></asp:Label></td>
<td width="130px" align="right" 
        style="background-color: #000000; font-weight: bold; color: #FFFFFF;" 
        class="style13">&nbsp;</td>
<td width="130px" align="right" 
        style="background-color: #000000; font-weight: bold; color: #FFFFFF;" 
        class="style7">
    <b>MTD</b></td>
<td width="130px" align="right" 
        style="background-color: #000000; font-weight: bold; color: #FFFFFF;" 
        class="style7">
    <b>YTD</b></td>
</tr>
<tr class="style15">
<td width="90px" 
        style="background-color: #000000; font-weight: bold; color: #FFFFFF; padding-top: 3px; padding-bottom: 3px;" 
        class="style9">Target</td>
<td width="130px" style="background-color: #FFFFFF" class="style13">
    <asp:Label ID="lblPB_Daily" runat="server" 
                   Text='<%# string.Format("{0:n2}",Eval("PLAN_DAILY")) %>' 
        Visible="False"></asp:Label></td>
<td width="130px" style="background-color: #FFFFFF" class="style13"><asp:Label ID="lblPB_Monthly" runat="server" 
                   Text='<%#string.Format("{0:n2}",Eval("PLAN_MONTHLY")) %>'>
                </asp:Label></td>
<td width="130px" style="background-color: #FFFFFF" class="style13"><asp:Label ID="lblPY_Monthly" runat="server" 
                   Text='<%#string.Format("{0:n2}",Eval("PLAN_YEARLY")) %>'>
                </asp:Label></td>
</tr>
<tr class="style15">
<td width="90px" 
        style="background-color: #000000; font-weight: bold; color: #FFFFFF;  padding-top: 3px; padding-bottom: 3px;" 
        class="style9">Actual</td>
<td width="130px" style="background-color: #FFFFFF" class="style13"><asp:Label 
        ID="lblDaily" runat="server" 
                   Text='<%# string.Format("{0:n2}",Eval("QTY_DAILY")) %>' 
        Visible="False"></asp:Label></td>
<td width="130px" style="background-color: #FFFFFF" class="style13"><asp:Label ID="lblMonthly" runat="server" 
                   Text='<%#string.Format("{0:n2}",Eval("QTY_MONTHLY")) %>'>
                </asp:Label></td>
<td width="130px" style="background-color: #FFFFFF" class="style13"><asp:Label ID="lblYearly" runat="server" 
                   Text='<%#string.Format("{0:n2}",Eval("QTY_YEARLY")) %>'>
                </asp:Label></td>
</tr>
<tr class="style15">
<td width="90px" 
        style="background-color: #000000; font-weight: bold; color: #FFFFFF;  padding-top: 3px; padding-bottom: 3px;" 
        class="style9">Percentage</td>
<td width="130px" style="background-color: #FFFFFF" class="style13">
    <asp:Label ID="Label41" runat="server" 
                   Font-Bold="True" Visible="False"></asp:Label></td>
<td width="130px" style="background-color: #FFFFFF" class="style13">
    <asp:Label ID="Label42" runat="server" 
                   Font-Bold="True" Width="100%"></asp:Label></td>
<td width="130px" style="background-color: #FFFFFF" class="style13">
    <asp:Label ID="Label43" runat="server" 
                   Font-Bold="True" Width="100%"></asp:Label></td>
</tr>
</table>
</ItemTemplate>
    <SeparatorStyle BackColor="#E3DFDF" />
    <SeparatorTemplate>
        
    </SeparatorTemplate>
</asp:DataList>
        <asp:DataList ID="DLSalesAll_Total" runat="server" BackColor="White" 
                        BorderColor="#DEDFDE" Width="520px" 
        BorderStyle="None" BorderWidth="2px" CellPadding="5" DataSourceID="DSSalesAll_Total" 
        ForeColor="Black" GridLines="Vertical" 
                        style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt" 
                    ShowFooter="False">
    <AlternatingItemStyle BackColor="#F5F5F5" />
    <FooterStyle BackColor="#F5F5F5" Height="0px" />
    <HeaderStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="Red" Height="0px" />
    <HeaderTemplate></HeaderTemplate>
    <ItemStyle BackColor="#F5F5F5" Font-Size="8pt" />
    <SelectedItemStyle BackColor="#F5F5F5" Font-Bold="True" ForeColor="White" />
<ItemTemplate>
<table border="0" cellpadding="0" cellspacing="0" style="padding: 0px 10px 0px 10px; background-color: #000000">
<tr style="margin: 0px; padding: 0px;" class="style15">
<td width="130px" align="left" 
        style="font-weight: bold; background-color: #000000; color: #FFFFFF;padding-top: 3px;  padding-bottom: 3px; ">
    TARGET</td>
<td width="130px" align="right" 
        style="font-weight: bold; background-color: #000000; color: #FFFFFF;">
    <asp:Label ID="Label24" runat="server" 
        Text='<%# string.Format("{0:n2}",Eval("PLAN_DAILY")) %>' Visible="False"></asp:Label>
    </td>
<td width="130px" align="right" 
        style="font-weight: bold; background-color: #000000; color: #FFFFFF;">
    <asp:Label ID="Label20" runat="server" 
        Text='<%# string.Format("{0:n2}",Eval("PLAN_MONTHLY")) %>'></asp:Label>
    </td>
<td width="130px" align="right" 
        style="font-weight: bold; background-color: #000000; color: #FFFFFF;">
    <asp:Label ID="Label21" runat="server" 
        Text='<%# string.Format("{0:n2}",Eval("PLAN_YEARLY")) %>'></asp:Label>
    </td>
</tr>
    <tr class="style15" style="margin: 0px; padding: 0px;">
        <td align="left" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;padding-top: 3px;  padding-bottom: 3px; " 
            width="130px">
            <asp:Label ID="Label1" runat="server" Text="ACTUAL"></asp:Label>
        </td>
        <td align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
            width="130px">
            <asp:Label ID="Label17" runat="server" 
                Text='<%# string.Format("{0:n2}",Eval("QTY_DAILY")) %>' Visible="False"></asp:Label>
        </td>
        <td align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
            width="130px">
            <asp:Label ID="Label18" runat="server" 
                Text='<%#string.Format("{0:n2}",Eval("Qty_Monthly")) %>'>
                </asp:Label>
        </td>
        <td align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
            width="130px">
            <asp:Label ID="Label19" runat="server" 
                Text='<%#string.Format("{0:n2}",Eval("Qty_Yearly")) %>'>
                </asp:Label>
        </td>
    </tr>
    <tr class="style15" style="margin: 0px; padding: 0px;">
        <td align="left" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;padding-top: 3px;  padding-bottom: 3px; " 
            width="130px">
            PERENTAGE</td>
        <td align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
            width="130px">
            &nbsp;</td>
        <td align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
            width="130px">
            <asp:Label ID="Label22" runat="server" 
                Text='<%# string.Format("{0:n2}",Eval("Qty_Yearly")) %>' Width="100%"></asp:Label>
        </td>
        <td align="right" 
            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
            width="130px">
            <asp:Label ID="Label23" runat="server" 
                Text='<%# string.Format("{0:n2}",Eval("Qty_Yearly")) %>' Width="100%"></asp:Label>
        </td>
    </tr>
</table>
</ItemTemplate>
    <SeparatorStyle BackColor="#F5F5F5" Height="10px" />
    <SeparatorTemplate>
        
    </SeparatorTemplate>
</asp:DataList>
    
        </td></tr>
    <tr><td></td><td>&nbsp;</td><td>&nbsp;</td></tr>
    <tr align="center">
    <td colspan="3">
    
        <asp:DataList ID="DLCoalStockMerapi" runat="server" BackColor="White" BorderColor="Black" 
            BorderStyle="Solid" BorderWidth="5px" CellPadding="5" DataSourceID="DSCoalStockMerapi" 
            ForeColor="Black" GridLines="Horizontal" ShowFooter="False" 
            style="font-family: Arial, Helvetica, sans-serif; font-size: small" 
            Width="520px">
            <AlternatingItemStyle BackColor="WhiteSmoke" />
            <FooterStyle BackColor="WhiteSmoke" />
            <HeaderStyle BackColor="WhiteSmoke" Font-Bold="True" Font-Size="12pt" 
                ForeColor="Red" Height="10px" />
            <HeaderTemplate>
                <asp:Label ID="Label7" runat="server" Font-Size="X-Large">Coal Stock</asp:Label>
            </HeaderTemplate>
            <ItemStyle BackColor="WhiteSmoke" Font-Size="8pt" />
            <ItemTemplate>
                <table border="0" cellpadding="3" cellspacing="0" 
                    style="padding:0px 10px 0px 10px; background-color: WhiteSmoke">
                    <tr>
                        <td align="left" 
                            style="font-weight: bold; background-color: #555555; color: #FFFFFF; " 
                            >
                            Location</td>
                        <td align="right" 
                            style="font-weight: bold; background-color: #555555; color: #FFFFFF; text-align: left;" 
                           >
                            Coal Stock</td>
                        <td align="right" 
                            style="font-weight: bold; background-color: #555555; color: #FFFFFF; right: 3px; text-align: right;" 
                            >
                            Nett Stock
        <br />
                            GAR 4800</td>
                        <td align="right" 
                            style="font-weight: bold; background-color: #555555; color: #FFFFFF; right: 3px; text-align: right;" 
                            >
                            Nett Stock GAR 5300</td>
                        <td align="right" 
                            style="font-weight: bold; background-color: #555555; color: #FFFFFF; right: 3px; text-align: right;">
                            Nett Stock GAR 4100</td>
                        <td align="right" 
                            style="font-weight: bold; background-color: #555555; color: #FFFFFF; right: 3px; text-align: right;">
                            Nett Stock GAR 4100</td>
                        <td align="right" 
                            style="font-weight: bold; background-color: #555555; color: #FFFFFF; right: 3px; text-align: right;" 
                           >
                            Bedding <br />(Based on <br />Independent Surveyor)</td>
                        <td align="right" 
                            style="font-weight: bold; background-color: #555555; color: #FFFFFF; right: 3px; text-align: right;" 
                            >
                            Total</td>
                    </tr>
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                            >
                            MERAPI</td>
                        <td class="style10" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" 
                            >
                            ROM</td>
                        <td class="style13" style="background-color: #FFFFFF; color: #000000;" id="merapirom_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CMRRX")) %>
                        </td>
                        <td class="style13" style="background-color: #FFFFFF; color: #000000;" id="merapirom_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CMRRC")) %>
                        </td>
                        <td class="style13" style="background-color: #FFFFFF; color: #000000;" id="merapirom_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CMRRY")) %>
                        </td>
                        <td class="style13" style="background-color: #FFFFFF; color: #000000;" id="merapirom_gar4800b">
                            0
                        </td>
                        <td class="style13" style="background-color: #FFFFFF; color: #000000;" id="merapirom_bedding"
                           >
                            <%#string.Format("{0:n2}",Eval("B_CMRR")) %>
                        </td>
                        <td class="style13" style="background-color: #FFFFFF; color: #000000;" id="merapirom_total">
                            <%#string.Format("{0:n2}",Eval("Stock_CMRRX") + Eval("Stock_CMRRC") + Eval("Stock_CMRRY")) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                            >
                            &nbsp;</td>
                        <td class="style10" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" 
                            >
                            CRUSHED</td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="merapicrushed_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CMRCX")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="merapicrushed_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CMRCC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="merapicrushed_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CMRCY")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="merapicrushed_gar4800b">
                            <%# String.Format("{0:n2}", Eval("Dif_CMRCV"))%>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="merapicrushed_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CMRC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="merapicrushed_total">
                            <%# String.Format("{0:n2}", Eval("Stock_CMRCX") + Eval("Stock_CMRCC") + Eval("Stock_CMRCY") + Eval("Stock_CMRCV"))%>
                        </td>
                    </tr>
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                            >
                            &nbsp;</td>
                        <td class="style10" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" 
                            >
                            TOTAL</td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="merapitotal_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CMRRX") + Eval("Dif_CMRCX")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="merapitotal_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CMRRC") + Eval("Dif_CMRCC")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="merapitotal_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CMRRY") + Eval("Dif_CMRCY")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="merapitotal_gar4800b">
                            <%# String.Format("{0:n2}", 0 + Eval("Dif_CMRCV"))%>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="merapitotal_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CMRR") + Eval("B_CMRC")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="merapitotal_all">
                            <%#string.Format("{0:n2}",Eval("Stock_CMRRX") + Eval("Stock_CMRCX") + Eval("Stock_CMRRC") + Eval("Stock_CMRCC") + Eval("Stock_CMRRY") + Eval("Stock_CMRCY") + Eval("Stock_CMRCV")) %>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <SeparatorStyle BackColor="#E3DFDF" Height="10px" />
            <SeparatorTemplate>
            </SeparatorTemplate>
        </asp:DataList>
        <asp:DataList ID="DLCoalStockSukacinta" runat="server" BackColor="White" BorderColor="Black" 
            BorderStyle="Solid" BorderWidth="5px" CellPadding="5" DataSourceID="DSCoalStockSukacinta" 
            ForeColor="Black" GridLines="Horizontal" ShowFooter="False" 
            style="font-family: Arial, Helvetica, sans-serif; font-size: small" 
            Width="520px">
            <AlternatingItemStyle BackColor="WhiteSmoke" />
            <ItemStyle BackColor="WhiteSmoke" Font-Size="8pt" />
            <ItemTemplate>
                <table border="0" cellpadding="3" cellspacing="0" 
                    style="padding:0px 10px 0px 10px; background-color: WhiteSmoke">
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF; width:45px;" >
                            SUKA<br />CINTA</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" >
                            <b>ROM</b></td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:46px;" id="sukacintarom_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CSCRX")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:74px;" id="sukacintarom_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CSCRC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:75px;" id="sukacintarom_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CSCRY")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:97px;" id="sukacintarom_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CSCR")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:48px;" id="sukacintarom_total">
                            <%#string.Format("{0:n2}",Eval("Stock_CSCRX") + Eval("Stock_CSCRC") + Eval("Stock_CSCRY")) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" >
                            &nbsp;</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" >
                            <b>CRUSHED</b></td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="sukacintacrushed_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CSCCX")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="sukacintacrushed_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CSCCC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="sukacintacrushed_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CSCCY")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="sukacintacrushed_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CSCC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="sukacintacrushed_total">
                            <%#string.Format("{0:n2}",Eval("Stock_CSCCX") + Eval("Stock_CSCCC")  + Eval("Stock_CSCCY")) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" >
                            &nbsp;</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" >
                            <b>TOTAL</b></td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="sukacintatotal_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CSCCX") + Eval("Dif_CSCRX")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="sukacintatotal_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CSCCC") + Eval("Dif_CSCRC")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="sukacintatotal_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CSCCY") + Eval("Dif_CSCRY")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="sukacintatotal_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CSCC") + Eval("B_CSCR")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="sukacintatotal_all">
                            <%#string.Format("{0:n2}",Eval("Stock_CSCCX") + Eval("Stock_CSCRX") + Eval("Stock_CSCCC") + Eval("Stock_CSCRC") + Eval("Stock_CSCCY") + Eval("Stock_CSCRY")) %>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <SeparatorStyle BackColor="#E3DFDF" Height="10px" />
            <SeparatorTemplate>
            </SeparatorTemplate>
        </asp:DataList>


        <asp:DataList ID="DLCoalStockKertapati" runat="server" BackColor="White" BorderColor="Black" 
            BorderStyle="Solid" BorderWidth="5px" CellPadding="5" DataSourceID="DSCoalStockKertapati" 
            ForeColor="Black" GridLines="Horizontal" ShowFooter="False" 
            style="font-family: Arial, Helvetica, sans-serif; font-size: small" 
            Width="520px">
            <AlternatingItemStyle BackColor="WhiteSmoke" />
            <ItemStyle BackColor="WhiteSmoke" Font-Size="8pt" />
            <ItemTemplate>
                <table border="0" cellpadding="3" cellspacing="0" 
                    style="padding:0px 10px 0px 10px; background-color: WhiteSmoke">
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF; width:45px;" >
                            KERTA<br />PATI</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" >
                            <b>ROM</b></td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:46px;" id="kertapatirom_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CKTRX")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:74px;" id="kertapatirom_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CKTRC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:75px;" id="kertapatirom_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CKTRY")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:97px;" id="kertapatirom_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CKTR")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:48px;" id="kertapatirom_total">
                            <%#string.Format("{0:n2}",Eval("Stock_CKTRX") + Eval("Stock_CKTRC")  + Eval("Stock_CKTRY")) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" >
                            &nbsp;</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" >
                            <b>CRUSHED</b></td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="kertapaticrushed_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CKTCX")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="kertapaticrushed_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CKTCC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="kertapaticrushed_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CKTCY")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="kertapaticrushed_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CKTC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="kertapaticrushed_total">
                            <%#string.Format("{0:n2}",Eval("Stock_CKTCX") + Eval("Stock_CKTCC") + Eval("Stock_CKTCY")) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" >
                            &nbsp;</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" >
                            <b>TOTAL</b></td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="kertapatitotal_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CKTCX") + Eval("Dif_CKTRX")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="kertapatitotal_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CKTCC") + Eval("Dif_CKTRC")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="kertapatitotal_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CKTCY") + Eval("Dif_CKTRY")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="kertapatitotal_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CKTC") + Eval("B_CKTR")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="kertapatitotal_all">
                            <%#string.Format("{0:n2}",Eval("Stock_CKTCX") + Eval("Stock_CKTRX") + Eval("Stock_CKTCC") + Eval("Stock_CKTRC") + Eval("Stock_CKTCY") + Eval("Stock_CKTRY")) %>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <SeparatorStyle BackColor="#E3DFDF" Height="10px" />
            <SeparatorTemplate>
            </SeparatorTemplate>
        </asp:DataList>

        <asp:DataList ID="DLCoalStockEPI" runat="server" BackColor="White" BorderColor="Black" 
            BorderStyle="Solid" BorderWidth="5px" CellPadding="5" DataSourceID="DSCoalStockEPI" 
            ForeColor="Black" GridLines="Horizontal" ShowFooter="False" 
            style="font-family: Arial, Helvetica, sans-serif; font-size: small" 
            Width="520px">
            <AlternatingItemStyle BackColor="WhiteSmoke" />
            <ItemStyle BackColor="WhiteSmoke" Font-Size="8pt" />
            <ItemTemplate>
                <table border="0" cellpadding="3" cellspacing="0" 
                    style="padding:0px 10px 0px 10px; background-color: WhiteSmoke">
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF; width:45px;" >
                            EPI</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" >
                            <b>ROMS</b></td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:46px;" id="epirom_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CEPIRX")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:74px;" id="epirom_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CEPIRC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:75px;" id="epirom_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CEPIRY")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:97px;" id="epirom_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CEPIR")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:48px;" id="epirom_total">
                            <%#string.Format("{0:n2}",Eval("Stock_CEPIRX") + Eval("Stock_CEPIRC") + Eval("Stock_CEPIRY")) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                            >
                            &nbsp;</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" 
                            >
                            <b>CRUSHED</b></td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="epicrushed_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CEPICX")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="epicrushed_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CEPICC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="epicrushed_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CEPICY")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="epicrushed_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CEPIC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="epicrushed_total">
                            <%#string.Format("{0:n2}",Eval("Stock_CEPICX") + Eval("Stock_CEPICC") + Eval("Stock_CEPICY")) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" >
                            &nbsp;</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" >
                            <b>TOTAL</b></td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="epitotal_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CEPICX") + Eval("Dif_CEPIRX")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="epitotal_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CEPICC") + Eval("Dif_CEPIRC")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="epitotal_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CEPICY") + Eval("Dif_CEPIRY")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="epitotal_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CEPIC") + Eval("B_CEPIR")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="epitotal_all">
                            <%#string.Format("{0:n2}",Eval("Stock_CEPICX") + Eval("Stock_CEPIRX") + Eval("Stock_CEPICC") + Eval("Stock_CEPIRC") + Eval("Stock_CEPICY") + Eval("Stock_CEPIRY")) %>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <SeparatorStyle BackColor="#E3DFDF" Height="10px" />
            <SeparatorTemplate>
            </SeparatorTemplate>
        </asp:DataList>

        <asp:DataList ID="DLCoakStockRMK" runat="server" BackColor="White" BorderColor="Black" 
            BorderStyle="Solid" BorderWidth="5px" CellPadding="5" DataSourceID="DSCoalStockRMK" 
            ForeColor="Black" GridLines="Horizontal" ShowFooter="False" 
            style="font-family: Arial, Helvetica, sans-serif; font-size: small" 
            Width="520px">
            <AlternatingItemStyle BackColor="WhiteSmoke" />
            <ItemStyle BackColor="WhiteSmoke" Font-Size="8pt" />
            <ItemTemplate>
                <table border="0" cellpadding="3" cellspacing="0" 
                    style="padding:0px 10px 0px 10px; background-color: WhiteSmoke">
                    <tr>
                        <td class="style9" style="font-weight: bold; background-color: #000000; color: #FFFFFF; width:45px;">
                            RMK</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" 
                            >
                            <b>ROM</b></td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; width:46px;" id="rmkrom_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CRMKRX")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; min-width:74px;"  id="rmkrom_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CRMKRC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; min-width:75px;" id="rmkrom_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CRMKRY")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; min-width:97px;"  id="rmkrom_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CRMKR")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; min-width:48px;" id="rmkrom_total">
                            <%#string.Format("{0:n2}",Eval("Stock_CRMKRX") + Eval("Stock_CRMKRC") + Eval("Stock_CRMKRY")) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" >
                            &nbsp;</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" >
                            <b>CRUSHED</b></td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="rmkcrushed_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CRMKCX")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="rmkcrushed_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CRMKCC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="rmkcrushed_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CRMKCY")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="rmkcrushed_bedding" >
                            <%#string.Format("{0:n2}",Eval("B_CRMKC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;"  id="rmkcrushed_total" >
                            <%#string.Format("{0:n2}",Eval("Stock_CRMKCX") + Eval("Stock_CRMKCC") + Eval("Stock_CRMKCY")) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="style12" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" >
                            &nbsp;</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" >
                            <b>TOTAL</b></td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="rmktotal_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CRMKCX") + Eval("Dif_CRMKRX")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="rmktotal_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CRMKCC") + Eval("Dif_CRMKRC")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="rmktotal_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CRMKCY") + Eval("Dif_CRMKRY")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="rmktotal_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CRMKC") + Eval("B_CRMKR")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="rmktotal_all">
                            <%#string.Format("{0:n2}",Eval("Stock_CRMKCX") + Eval("Stock_CRMKRX") + Eval("Stock_CRMKCC") + Eval("Stock_CRMKRC") + Eval("Stock_CRMKCY") + Eval("Stock_CRMKRY")) %>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <SeparatorStyle BackColor="#E3DFDF" Height="10px" />
            <SeparatorTemplate>
            </SeparatorTemplate>
        </asp:DataList>

        <asp:DataList ID="DLCoakStockSDJ" runat="server" BackColor="White" BorderColor="Black" 
            BorderStyle="Solid" BorderWidth="5px" CellPadding="5" DataSourceID="DSCoalStockSDJ" 
            ForeColor="Black" GridLines="Horizontal" ShowFooter="False" 
            style="font-family: Arial, Helvetica, sans-serif; font-size: small" 
            Width="520px">
            <AlternatingItemStyle BackColor="WhiteSmoke" />
            <ItemStyle BackColor="WhiteSmoke" Font-Size="8pt" />
            <ItemTemplate>
                <table border="0" cellpadding="3" cellspacing="0" 
                    style="padding:0px 10px 0px 10px; background-color: WhiteSmoke">
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF; width:45px;" >
                            SDJ</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" >
                            <b>ROM</b></td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; min-width:46px;" id="sdjrom_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CSDJRX")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; min-width:74px;" id="sdjrom_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CSDJRC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; min-width:75px;" id="sdjrom_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CSDJRY")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; min-width:97px;" id="sdjrom_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CSDJR")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; min-width:48px;" id="sdjrom_total">
                            <%#string.Format("{0:n2}",Eval("Stock_CSDJRX") + Eval("Stock_CSDJRC") + Eval("Stock_CSDJRY")) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" >
                            &nbsp;</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" >
                            <b>CRUSHED</b></td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="sdjcrushed_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CSDJCX")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="sdjcrushed_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CSDJCC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="sdjcrushed_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CSDJCY")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="sdjcrushed_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CSDJC")) %>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="sdjcrushed_total">
                            <%#string.Format("{0:n2}",Eval("Stock_CSDJCX") + Eval("Stock_CSDJCC") + Eval("Stock_CSDJCY")) %>
                        </td>
                    </tr>
                    <tr>
                        <td class="style12" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                            >
                            &nbsp;</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" 
                           >
                            <b>TOTAL</b></td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="sdjtotal_alam123">
                            <%#string.Format("{0:n2}",Eval("Dif_CSDJCX") + Eval("Dif_CSDJRX")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="sdjtotal_alam4">
                            <%#string.Format("{0:n2}",Eval("Dif_CSDJCC") + Eval("Dif_CSDJRC")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="sdjtotal_alam5">
                            <%#string.Format("{0:n2}",Eval("Dif_CSDJCY") + Eval("Dif_CSDJRY")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="sdjtotal_bedding">
                            <%#string.Format("{0:n2}",Eval("B_CSDJC") + Eval("B_CSDJR")) %>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="sdjtotal_all">
                            <%#string.Format("{0:n2}",Eval("Stock_CSDJCX") + Eval("Stock_CSDJRX") + Eval("Stock_CSDJCC") + Eval("Stock_CSDJRC") + Eval("Stock_CSDJCY") + Eval("Stock_CSDJRY")) %>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <SeparatorStyle BackColor="#E3DFDF" Height="10px" />
            <SeparatorTemplate>
            </SeparatorTemplate>
        </asp:DataList>

        <asp:DataList ID="DLCoakStockTJB" runat="server" BackColor="White" BorderColor="Black" 
            BorderStyle="Solid" BorderWidth="5px" CellPadding="5" DataSourceID="DSCoalStockTJB" 
            ForeColor="Black" GridLines="Horizontal" ShowFooter="False" 
            style="font-family: Arial, Helvetica, sans-serif; font-size: small" 
            Width="520px">
            <AlternatingItemStyle BackColor="WhiteSmoke" />
            <ItemStyle BackColor="WhiteSmoke" Font-Size="8pt" />
            <ItemTemplate>
                <table border="0" cellpadding="3" cellspacing="0" 
                    style="padding:0px 10px 0px 10px; background-color: WhiteSmoke">
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF; width:45px;" >
                            TJB</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" >
                            <b>ROM</b></td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; min-width:46px;" id="TBrom_alam123">
                            <%# String.Format("{0:n2}", Eval("Dif_CTJBRX"))%>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; min-width:74px;" id="TBrom_alam4">
                            <%# String.Format("{0:n2}", Eval("Dif_CTJBRC"))%>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; min-width:75px;" id="TBrom_alam5">
                            <%# String.Format("{0:n2}", Eval("Dif_CTJBRY"))%>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; min-width:97px;" id="TBrom_bedding">
                            <%# String.Format("{0:n2}", Eval("B_CTJBR"))%>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000; min-width:48px;" id="TBrom_total">
                            <%# String.Format("{0:n2}", Eval("Stock_CTJBRX") + Eval("Stock_CTJBRC") + Eval("Stock_CTJBRY"))%>
                        </td>
                    </tr>
                    <tr>
                        <td class="style9" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" >
                            &nbsp;</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" >
                            <b>CRUSHED</b></td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="TBcrushed_alam123">
                            <%# String.Format("{0:n2}", Eval("Dif_CTJBCX"))%>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="TBcrushed_alam4">
                            <%# String.Format("{0:n2}", Eval("Dif_CTJBCC"))%>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="TBcrushed_alam5">
                            <%# String.Format("{0:n2}", Eval("Dif_CTJBCY"))%>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="TBcrushed_bedding">
                            <%# String.Format("{0:n2}", Eval("B_CTJBC"))%>
                        </td>
                        <td align="right" style="background-color: #FFFFFF; color: #000000;" id="TBcrushed_total">
                            <%# String.Format("{0:n2}", Eval("Stock_CTJBCX") + Eval("Stock_CTJBCC") + Eval("Stock_CTJBCY"))%>
                        </td>
                    </tr>
                    <tr>
                        <td class="style12" 
                            style="font-weight: bold; background-color: #000000; color: #FFFFFF;" 
                            >
                            &nbsp;</td>
                        <td class="style9" 
                            style="background-color: #555555; font-weight: bold; color: #FFFFFF;" 
                           >
                            <b>TOTAL</b></td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="TBtotal_alam123">
                            <%# String.Format("{0:n2}", Eval("Dif_CTJBCX") + Eval("Dif_CTJBRX"))%>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="TBtotal_alam4">
                            <%# String.Format("{0:n2}", Eval("Dif_CTJBCC") + Eval("Dif_CTJBRC"))%>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="TBtotal_alam5">
                            <%# String.Format("{0:n2}", Eval("Dif_CTJBCY") + Eval("Dif_CTJBRY"))%>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="TBtotal_bedding">
                            <%# String.Format("{0:n2}", Eval("B_CTJBC") + Eval("B_CTJBR"))%>
                        </td>
                        <td align="right" 
                            style="background-color: #999999; color: #000000; font-weight: bold;" id="TBtotal_all">
                            <%# String.Format("{0:n2}", Eval("Stock_CTJBCX") + Eval("Stock_CTJBRX") + Eval("Stock_CTJBCC") + Eval("Stock_CTJBRC") + Eval("Stock_CTJBCY") + Eval("Stock_CTJBRY"))%>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <SeparatorStyle BackColor="#E3DFDF" Height="10px" />
            <SeparatorTemplate>
            </SeparatorTemplate>
        </asp:DataList>

        <!-- TOTAL COALSTOCK -->
       <%-- <AlternatingItemStyle BackColor="WhiteSmoke" />
            <ItemStyle BackColor="WhiteSmoke" Font-Size="5pt" />
            <ItemTemplate>--%>
        <table border="0" cellpadding="3" cellspacing="0" 
                    style="padding:0px 10px 0px 10px; background-color: WhiteSmoke; font-size:9pt;">
            <tr style="margin: 0px; padding: 0px;">
                <td align="left" height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF;" >
                    GRAND</td>
                <td class="style9" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " >
                    ROM</td>
                <td align="right" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " id="totalrom_alam123">
                </td>
                <td align="right" height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " id="totalrom_alam4">
                </td>
                <td class="style13" height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " id="totalrom_alam5">
                </td>
                <td height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " class="style13" id="totalrom_bedding">
                </td>
                <td height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " class="style13" id="grand_totalrom">
                </td>
            </tr>
            <tr style="margin: 0px; padding: 0px;">
                <td align="left" height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF;" >
                    TOTAL</td>
                <td class="style9" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " >
                    CRUSHED</td>
                <td align="right" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; width:46px;" id="totalcrushed_alam123">
                </td>
                <td height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; width:74px;" class="style13" id="totalcrushed_alam4">
                </td>
                <td class="style13" height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; width:75px; " id="totalcrushed_alam5">
                </td>
                <td height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; width:97px;" class="style13" id="totalcrushed_bedding">
                </td>
                <td height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; width:48px;" class="style13" id="grand_totalcrushed">
                </td>
            </tr>
            <tr style="margin: 0px; padding: 0px;">
                <td align="left" height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF;" >
                    &nbsp;</td>
                <td class="style9" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " >
                    TOTAL</td>
                <td align="right" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " id="totalall_alam123">
                </td>
                <td height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " class="style13" id="totalall_alam4">
                </td>
                <td class="style13" height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " id="totalall_alam5">
                </td>
                <td height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " class="style13" id="totalall_bedding">
                </td>
                <td height="15px" 
                    style="font-weight: bold; background-color: #000000; color: #FFFFFF; " class="style13" id="grand_totalall">
                </td>
            </tr>
        </table>
        <%--</ItemTemplate>
        <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
        <SeparatorStyle BackColor="#E3DFDF" Height="10px" />
        <SeparatorTemplate>
        </SeparatorTemplate>--%>
    </td>
    </tr>
    <tr>
    <td colspan="3">&nbsp
    </td></tr>
     <tr>
    <td colspan="3">&nbsp
    </td></tr>
    <tr>
    <td colspan="3" style="text-align: left">
        
          <b>Note:</b> <br />1. Pencatatan data Barging harus menunggu laporan Final Draft Survey, max keterlambatan : 1 hari <br />
    2. Pencatatan data Hauling di Stockpile RMK dan EPI harus menunggu pengiriman data dari RMK dan EPI, max keterlambatan : 3 hari
    </td>
    </tr>
    </table>

    <asp:SqlDataSource ID="DSProduction" runat="server" 
            ConflictDetection="CompareAllValues" 
            ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
            ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
            SelectCommand="sp_prd_bidb_production123"
            SelectCommandType="StoredProcedure"
            OldValuesParameterFormatString="Original_{0}" >
            <SelectParameters>
                <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
                <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
                <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
            </SelectParameters>
        </asp:SqlDataSource>

    <asp:SqlDataSource ID="DSProductionTotal" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_production123_total"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="DSProduction4" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_production4"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
     
    <asp:SqlDataSource ID="DSProductionTotal4" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_production4_total"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="DSProduction5" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_production5"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="DSProductionTotal5" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_production5_total"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="DSProductionTotalAll" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_productionall_total"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
   
    <asp:SqlDataSource ID="DSCrushing" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>"  
        SelectCommand="sp_prd_bidb_crushing"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="DSCrushingTotal" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_crushing_total"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="DSHauling" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_hauling"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="DSHaulingTotal" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>"  
        SelectCommand="sp_prd_bidb_hauling_total"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="DSSales" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_sales123"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="DSSales_Total" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_sales123_total"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="DSSales4" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_sales4"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="DSSales_Total4" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_sales4_total"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>

    <!--- Barging 5 -->
    <asp:SqlDataSource ID="DSSales5" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_sales5"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
        
     <asp:SqlDataSource ID="DSSales_Total5" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_sales5_total"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
    <!--- End Barging 5 -->
    
    <asp:SqlDataSource ID="DSSalesAll" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_salesall"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="DSSalesAll_Total" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_salesall_total"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="TM" QueryStringField="TM" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>

    <!--- Coal Stock -->
    <asp:SqlDataSource ID="DSStock" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_coalstock_total"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
    <!--- End Coal Stock -->

    <asp:SqlDataSource ID="DSCoalStockMerapi" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_coalstockmerapi"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="DSCoalStockSukacinta" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_coalstocksukacinta"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="DSCoalStockKertapati" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_coalstockkertapati"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="DSCoalStockEPI" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_coalstockepi"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="DSCoalStockRMK" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_coalstockrmk"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="DSCoalStockSDJ" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_coalstocksdj"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>

    
    <asp:SqlDataSource ID="DSCoalStockTJB" runat="server" 
        ConflictDetection="CompareAllValues" 
        ConnectionString="<%$ ConnectionStrings:MASConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:MASConnectionString.ProviderName %>" 
        SelectCommand="sp_prd_bidb_coalstocktjb"
        SelectCommandType="StoredProcedure"
        OldValuesParameterFormatString="Original_{0}" >
        <SelectParameters>
            <asp:QueryStringParameter Name="MDateT" QueryStringField="MDateT" />
            <asp:QueryStringParameter Name="Y" QueryStringField="Y" />
        </SelectParameters>
    </asp:SqlDataSource>
    </form>
    <script>
        //rom
        document.getElementById("totalrom_alam123").innerHTML = (Number(document.getElementById("merapirom_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("sukacintarom_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("kertapatirom_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("epirom_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("rmkrom_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("sdjrom_alam123").innerHTML.replace(",", ""))).toFixed(2);
        document.getElementById("totalrom_alam4").innerHTML = (Number(document.getElementById("merapirom_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("sukacintarom_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("kertapatirom_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("epirom_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("rmkrom_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("sdjrom_alam4").innerHTML.replace(",", ""))).toFixed(2);
        document.getElementById("totalrom_alam5").innerHTML = (Number(document.getElementById("merapirom_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("sukacintarom_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("kertapatirom_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("epirom_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("rmkrom_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("sdjrom_alam5").innerHTML.replace(",", ""))).toFixed(2);
        document.getElementById("totalrom_bedding").innerHTML = (Number(document.getElementById("merapirom_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("sukacintarom_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("kertapatirom_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("epirom_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("rmkrom_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("sdjrom_bedding").innerHTML.replace(",", ""))).toFixed(2);
        document.getElementById("grand_totalrom").innerHTML = (Number(document.getElementById("merapirom_total").innerHTML.replace(",", ""))
        + Number(document.getElementById("sukacintarom_total").innerHTML.replace(",", ""))
        + Number(document.getElementById("kertapatirom_total").innerHTML.replace(",", ""))
        + Number(document.getElementById("epirom_total").innerHTML.replace(",", ""))
        + Number(document.getElementById("rmkrom_total").innerHTML.replace(",", ""))
        + Number(document.getElementById("sdjrom_total").innerHTML.replace(",", ""))).toFixed(2);

        //crushed
        document.getElementById("totalcrushed_alam123").innerHTML = (Number(document.getElementById("merapicrushed_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("sukacintacrushed_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("kertapaticrushed_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("epicrushed_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("rmkcrushed_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("sdjcrushed_alam123").innerHTML.replace(",", ""))).toFixed(2);
        document.getElementById("totalcrushed_alam4").innerHTML = (Number(document.getElementById("merapicrushed_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("sukacintacrushed_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("kertapaticrushed_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("epicrushed_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("rmkcrushed_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("sdjcrushed_alam4").innerHTML.replace(",", ""))).toFixed(2);
        document.getElementById("totalcrushed_alam5").innerHTML = (Number(document.getElementById("merapicrushed_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("sukacintacrushed_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("kertapaticrushed_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("epicrushed_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("rmkcrushed_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("sdjcrushed_alam5").innerHTML.replace(",", ""))).toFixed(2);
        document.getElementById("totalcrushed_bedding").innerHTML = (Number(document.getElementById("merapicrushed_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("sukacintacrushed_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("kertapaticrushed_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("epicrushed_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("rmkcrushed_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("sdjcrushed_bedding").innerHTML.replace(",", ""))).toFixed(2);
        document.getElementById("grand_totalcrushed").innerHTML = (Number(document.getElementById("merapicrushed_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("sukacintacrushed_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("kertapaticrushed_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("epicrushed_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("rmkcrushed_alam123").innerHTML.replace(",", ""))
         + Number(document.getElementById("sdjcrushed_alam123").innerHTML.replace(",", ""))).toFixed(2);
        document.getElementById("totalcrushed_alam4").innerHTML = (Number(document.getElementById("merapicrushed_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("sukacintacrushed_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("kertapaticrushed_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("epicrushed_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("rmkcrushed_alam4").innerHTML.replace(",", ""))
         + Number(document.getElementById("sdjcrushed_alam4").innerHTML.replace(",", ""))).toFixed(2);
        document.getElementById("totalcrushed_alam5").innerHTML = (Number(document.getElementById("merapicrushed_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("sukacintacrushed_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("kertapaticrushed_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("epicrushed_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("rmkcrushed_alam5").innerHTML.replace(",", ""))
         + Number(document.getElementById("sdjcrushed_alam5").innerHTML.replace(",", ""))).toFixed(2);
        document.getElementById("totalcrushed_bedding").innerHTML = (Number(document.getElementById("merapicrushed_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("sukacintacrushed_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("kertapaticrushed_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("epicrushed_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("rmkcrushed_bedding").innerHTML.replace(",", ""))
         + Number(document.getElementById("sdjcrushed_bedding").innerHTML.replace(",", ""))).toFixed(2);
        document.getElementById("grand_totalcrushed").innerHTML = (Number(document.getElementById("merapicrushed_total").innerHTML.replace(",", ""))
        + Number(document.getElementById("sukacintacrushed_total").innerHTML.replace(",", ""))
        + Number(document.getElementById("kertapaticrushed_total").innerHTML.replace(",", ""))
        + Number(document.getElementById("epicrushed_total").innerHTML.replace(",", ""))
        + Number(document.getElementById("rmkcrushed_total").innerHTML.replace(",", ""))
        + Number(document.getElementById("sdjcrushed_total").innerHTML.replace(",", ""))).toFixed(2);
        document.getElementById("totalall_alam123").innerHTML = formatNumber((Number(document.getElementById("totalcrushed_alam123").innerHTML) + Number(document.getElementById("totalrom_alam123").innerHTML)).toFixed(2));
        document.getElementById("totalall_alam4").innerHTML = formatNumber((Number(document.getElementById("totalcrushed_alam4").innerHTML) + Number(document.getElementById("totalrom_alam4").innerHTML)).toFixed(2));
        document.getElementById("totalall_alam5").innerHTML = formatNumber((Number(document.getElementById("totalcrushed_alam5").innerHTML) + Number(document.getElementById("totalrom_alam5").innerHTML)).toFixed(2));
        document.getElementById("totalall_bedding").innerHTML = formatNumber((Number(document.getElementById("totalcrushed_bedding").innerHTML) + Number(document.getElementById("totalrom_bedding").innerHTML)).toFixed(2));
        document.getElementById("grand_totalall").innerHTML = formatNumber((Number(document.getElementById("grand_totalcrushed").innerHTML) + Number(document.getElementById("grand_totalrom").innerHTML)).toFixed(2));


        document.getElementById("totalrom_alam123").innerHTML = formatNumber(document.getElementById("totalrom_alam123").innerHTML);
        document.getElementById("totalrom_alam4").innerHTML = formatNumber(document.getElementById("totalrom_alam4").innerHTML);
        document.getElementById("totalrom_alam5").innerHTML = formatNumber(document.getElementById("totalrom_alam5").innerHTML);
        document.getElementById("totalrom_bedding").innerHTML = formatNumber(document.getElementById("totalrom_bedding").innerHTML);
        document.getElementById("grand_totalrom").innerHTML = formatNumber(document.getElementById("grand_totalrom").innerHTML);

        document.getElementById("totalcrushed_alam123").innerHTML = formatNumber(document.getElementById("totalcrushed_alam123").innerHTML);
        document.getElementById("totalcrushed_alam4").innerHTML = formatNumber(document.getElementById("totalcrushed_alam4").innerHTML);
        document.getElementById("totalcrushed_alam5").innerHTML = formatNumber(document.getElementById("totalcrushed_alam5").innerHTML);
        document.getElementById("totalcrushed_bedding").innerHTML = formatNumber(document.getElementById("totalcrushed_bedding").innerHTML);
        document.getElementById("grand_totalcrushed").innerHTML = formatNumber(document.getElementById("grand_totalcrushed").innerHTML);



        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
        }
    </script>
</body>
</html>

