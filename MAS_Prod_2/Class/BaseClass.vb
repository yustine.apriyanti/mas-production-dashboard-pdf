﻿Imports MySql.Data.MySqlClient
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates

Public Class BaseClass
    Inherits System.Web.UI.Page

    'Dim conString = ConfigurationManager.ConnectionStrings("MASConnectionString").ConnectionString()
    'Dim strConnString As String = conString.ConnectionString()
    Dim oConn As New MySqlConnection(ConfigurationManager.ConnectionStrings("MASConnectionString").ConnectionString())

    Public Function GetSMTPData(ByVal smtp As SmtpClient) As SmtpClient
        oConn.Open()
        Dim host As String = ""
        Dim email_from As String = ""
        Dim password As String = ""
        Dim port As Integer
        Dim oDatareader As MySqlDataReader
        Dim sqlCmd_host As New MySqlCommand("select * from mn_code_list where CatID = 'EMAIL' and Code = 'HOST' and Active = 1 ", oConn)
        oDatareader = sqlCmd_host.ExecuteReader()
        If oDatareader.Read() Then
            host = oDatareader("Description").ToString()
        End If
        oDatareader.Close()
        Dim sqlCmd_port As New MySqlCommand("select * from mn_code_list where CatID = 'EMAIL' and Code = 'PORT' and Active = 1 ", oConn)
        oDatareader = sqlCmd_port.ExecuteReader()
        If oDatareader.Read() Then
            port = Integer.Parse(oDatareader("Description").ToString())
        End If
        oDatareader.Close()
        Dim sqlCmd_email_from As New MySqlCommand("select * from mn_code_list where CatID = 'EMAIL' and Code = 'FROM' and Active = 1 ", oConn)
        oDatareader = sqlCmd_email_from.ExecuteReader()
        If oDatareader.Read() Then
            email_from = oDatareader("Description").ToString()
        End If
        oDatareader.Close()
        Dim sqlCmd_email_pass As New MySqlCommand("select * from mn_code_list where CatID = 'EMAIL' and Code = 'PASS' and Active = 1 ", oConn)
        oDatareader = sqlCmd_email_pass.ExecuteReader()
        If oDatareader.Read() Then
            password = oDatareader("Description").ToString()
        End If
        oDatareader.Close()
        oConn.Close()

        smtp.Host = host
        smtp.Port = port
        smtp.Credentials = New System.Net.NetworkCredential(email_from, password)
        smtp.EnableSsl = True
        ServicePointManager.ServerCertificateValidationCallback = Function(ByVal s As SmtpClient, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal sslPolicyErrors As SslPolicyErrors) True

        Return smtp
    End Function
End Class
