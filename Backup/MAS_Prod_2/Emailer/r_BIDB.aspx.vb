﻿Imports System.Collections
Imports System.Reflection
Imports System.Net
Imports System.Net.Dns

Public Class r_BIDB
    Inherits System.Web.UI.Page

    Dim opt As Integer = Nothing
    Dim opt_date As DateTime
    Dim Last_Month As String, This_Month As String, Y As String, Prev_Day As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim MDateT As Date = Date.Parse(Request.QueryString("MDateT")) 'MDateT=2018-11-28&LM=201810&TM=201811&Y=2018
        lblDate.Text = "Data per " & MDateT.ToString("dd/MM/yyyy")
        'If Session("iframe") = "" Then
        '    lblDate.Text = "Data per " & DateAdd(DateInterval.Day, -1, Now).ToString("dd/MM/yyyy")
        'Else
        '    lblDate.Text = "Data per " & CDate(Session("iframe")).ToString("dd/MM/yyyy")
        'End If
    End Sub

    Protected Sub DLProduction_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLProduction.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim Plan_Daily_CP As Label = CType(e.Item.FindControl("Label1"), Label)
            Dim Plan_Monthly_CP As Label = CType(e.Item.FindControl("Label2"), Label)
            Dim Plan_Yearly_CP As Label = CType(e.Item.FindControl("Label3"), Label)
            Dim Qty_Daily_CP As Label = CType(e.Item.FindControl("Label85"), Label)
            Dim Qty_Monthly_CP As Label = CType(e.Item.FindControl("Label5"), Label)
            Dim Qty_Yearly_CP As Label = CType(e.Item.FindControl("Label6"), Label)
            Dim cgd As Label = CType(e.Item.FindControl("Label26"), Label)
            Dim cgm As Label = CType(e.Item.FindControl("Label27"), Label)
            Dim cgy As Label = CType(e.Item.FindControl("Label28"), Label)


            If Not Qty_Daily_CP.Text = "" And Not Plan_Daily_CP.Text = "" Then
                cgd.Text = String.Format("{0:n2}", Qty_Daily_CP.Text / Plan_Daily_CP.Text * 100)
            Else
                cgd.Text = "0"
            End If

            If Not Qty_Monthly_CP.Text = "" And Not Plan_Monthly_CP.Text = "" Then
                cgm.Text = String.Format("{0:n2}", Qty_Monthly_CP.Text / Plan_Monthly_CP.Text * 100)
            Else
                cgm.Text = "0"
            End If

            If Not Qty_Yearly_CP.Text = "" And Not Plan_Yearly_CP.Text = "" Then
                cgy.Text = String.Format("{0:n2}", Qty_Yearly_CP.Text / Plan_Yearly_CP.Text * 100)
            Else
                cgy.Text = "0"
            End If


            If cgd.Text >= 100 Then
                cgd.ForeColor = Drawing.Color.White
                cgd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgd.Text > 79 And cgd.Text < 100 Then
                cgd.ForeColor = Drawing.Color.Black
                cgd.BackColor = Drawing.Color.Yellow
            Else
                cgd.ForeColor = Drawing.Color.White
                cgd.BackColor = Drawing.Color.Red
            End If

            If cgm.Text >= 100 Then
                cgm.ForeColor = Drawing.Color.White
                cgm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgm.Text > 79 And cgm.Text < 100 Then
                cgm.ForeColor = Drawing.Color.Black
                cgm.BackColor = Drawing.Color.Yellow
            Else
                cgm.ForeColor = Drawing.Color.White
                cgm.BackColor = Drawing.Color.Red
            End If

            If cgy.Text >= 100 Then
                cgy.ForeColor = Drawing.Color.White
                cgy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgy.Text > 79 And cgy.Text < 100 Then
                cgy.ForeColor = Drawing.Color.Black
                cgy.BackColor = Drawing.Color.Yellow
            Else
                cgy.ForeColor = Drawing.Color.White
                cgy.BackColor = Drawing.Color.Red
            End If

            cgd.Text = cgd.Text & " %"
            cgm.Text = cgm.Text & " %"
            cgy.Text = cgy.Text & " %"



            Dim Plan_Daily_OB As Label = CType(e.Item.FindControl("lblPlan_Daily"), Label)
            Dim Plan_Monthly_OB As Label = CType(e.Item.FindControl("lblPlan_Monthly"), Label)
            Dim Plan_Yearly_OB As Label = CType(e.Item.FindControl("lblPlan_Yearly"), Label)
            Dim Qty_Daily_OB As Label = CType(e.Item.FindControl("lblDaily_Actual"), Label)
            Dim Qty_Monthly_OB As Label = CType(e.Item.FindControl("lblMTD_Actual"), Label)
            Dim Qty_Yearly_OB As Label = CType(e.Item.FindControl("lblYTD_Actual"), Label)
            Dim obd As Label = CType(e.Item.FindControl("Label29"), Label)
            Dim obm As Label = CType(e.Item.FindControl("Label30"), Label)
            Dim oby As Label = CType(e.Item.FindControl("Label31"), Label)


            If Not Qty_Daily_OB.Text = "" And Not Plan_Daily_OB.Text = "" Then
                obd.Text = String.Format("{0:n2}", Qty_Daily_OB.Text / Plan_Daily_OB.Text * 100)
            Else
                obd.Text = "0"
            End If

            If Not Qty_Monthly_OB.Text = "" And Not Plan_Monthly_OB.Text = "" Then
                obm.Text = String.Format("{0:n2}", Qty_Monthly_OB.Text / Plan_Monthly_OB.Text * 100)
            Else
                obm.Text = "0"
            End If

            If Not Qty_Yearly_OB.Text = "" And Not Plan_Yearly_OB.Text = "" Then
                oby.Text = String.Format("{0:n2}", Qty_Yearly_OB.Text / Plan_Yearly_OB.Text * 100)
            Else
                oby.Text = "0"
            End If


            If obd.Text >= 100 Then
                obd.ForeColor = Drawing.Color.White
                obd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf obd.Text > 79 And obd.Text < 100 Then
                obd.ForeColor = Drawing.Color.Black
                obd.BackColor = Drawing.Color.Yellow
            Else
                obd.ForeColor = Drawing.Color.White
                obd.BackColor = Drawing.Color.Red
            End If

            If obm.Text >= 100 Then
                obm.ForeColor = Drawing.Color.White
                obm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf obm.Text > 79 And obm.Text < 100 Then
                obm.ForeColor = Drawing.Color.Black
                obm.BackColor = Drawing.Color.Yellow
            Else
                obm.ForeColor = Drawing.Color.White
                obm.BackColor = Drawing.Color.Red
            End If

            If oby.Text >= 100 Then
                oby.ForeColor = Drawing.Color.White
                oby.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf oby.Text > 79 And oby.Text < 100 Then
                oby.ForeColor = Drawing.Color.Black
                oby.BackColor = Drawing.Color.Yellow
            Else
                oby.ForeColor = Drawing.Color.White
                oby.BackColor = Drawing.Color.Red
            End If

            obd.Text = obd.Text & " %"
            obm.Text = obm.Text & " %"
            oby.Text = oby.Text & " %"

        End If
    End Sub

    Protected Sub DLProductionTotal_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLProductionTotal.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim Plan_Daily_CP As Label = CType(e.Item.FindControl("Label1"), Label)
            Dim Plan_Monthly_CP As Label = CType(e.Item.FindControl("Label2"), Label)
            Dim Plan_Yearly_CP As Label = CType(e.Item.FindControl("Label3"), Label)
            Dim Qty_Daily_CP As Label = CType(e.Item.FindControl("Label4"), Label)
            Dim Qty_Monthly_CP As Label = CType(e.Item.FindControl("Label5"), Label)
            Dim Qty_Yearly_CP As Label = CType(e.Item.FindControl("Label6"), Label)
            Dim cgd As Label = CType(e.Item.FindControl("Label32"), Label)
            Dim cgm As Label = CType(e.Item.FindControl("Label33"), Label)
            Dim cgy As Label = CType(e.Item.FindControl("Label34"), Label)


            If Not Qty_Daily_CP.Text = "" And Not Plan_Daily_CP.Text = "" Then
                cgd.Text = String.Format("{0:n2}", Qty_Daily_CP.Text / Plan_Daily_CP.Text * 100)
            Else
                cgd.Text = "0"
            End If

            If Not Qty_Monthly_CP.Text = "" And Not Plan_Monthly_CP.Text = "" Then
                cgm.Text = String.Format("{0:n2}", Qty_Monthly_CP.Text / Plan_Monthly_CP.Text * 100)
            Else
                cgm.Text = "0"
            End If

            If Not Qty_Yearly_CP.Text = "" And Not Plan_Yearly_CP.Text = "" Then
                cgy.Text = String.Format("{0:n2}", Qty_Yearly_CP.Text / Plan_Yearly_CP.Text * 100)
            Else
                cgy.Text = "0"
            End If


            If cgd.Text >= 100 Then
                cgd.ForeColor = Drawing.Color.White
                cgd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgd.Text > 79 And cgd.Text < 100 Then
                cgd.ForeColor = Drawing.Color.Black
                cgd.BackColor = Drawing.Color.Yellow
            Else
                cgd.ForeColor = Drawing.Color.White
                cgd.BackColor = Drawing.Color.Red
            End If

            If cgm.Text >= 100 Then
                cgm.ForeColor = Drawing.Color.White
                cgm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgm.Text > 79 And cgm.Text < 100 Then
                cgm.ForeColor = Drawing.Color.Black
                cgm.BackColor = Drawing.Color.Yellow
            Else
                cgm.ForeColor = Drawing.Color.White
                cgm.BackColor = Drawing.Color.Red
            End If

            If cgy.Text >= 100 Then
                cgy.ForeColor = Drawing.Color.White
                cgy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgy.Text > 79 And cgy.Text < 100 Then
                cgy.ForeColor = Drawing.Color.Black
                cgy.BackColor = Drawing.Color.Yellow
            Else
                cgy.ForeColor = Drawing.Color.White
                cgy.BackColor = Drawing.Color.Red
            End If

            cgd.Text = cgd.Text & " %"
            cgm.Text = cgm.Text & " %"
            cgy.Text = cgy.Text & " %"


            Dim Plan_Daily_OB As Label = CType(e.Item.FindControl("lblPlan_Daily"), Label)
            Dim Plan_Monthly_OB As Label = CType(e.Item.FindControl("lblPlan_Monthly"), Label)
            Dim Plan_Yearly_OB As Label = CType(e.Item.FindControl("lblPlan_Yearly"), Label)
            Dim Qty_Daily_OB As Label = CType(e.Item.FindControl("lblDaily_Actual"), Label)
            Dim Qty_Monthly_OB As Label = CType(e.Item.FindControl("lblMTD_Actual"), Label)
            Dim Qty_Yearly_OB As Label = CType(e.Item.FindControl("lblYTD_Actual"), Label)
            Dim obd As Label = CType(e.Item.FindControl("Label35"), Label)
            Dim obm As Label = CType(e.Item.FindControl("Label36"), Label)
            Dim oby As Label = CType(e.Item.FindControl("Label37"), Label)


            If Not Qty_Daily_OB.Text = "" And Not Plan_Daily_OB.Text = "" Then
                obd.Text = String.Format("{0:n2}", Qty_Daily_OB.Text / Plan_Daily_OB.Text * 100)
            Else
                obd.Text = "0"
            End If

            If Not Qty_Monthly_OB.Text = "" And Not Plan_Monthly_OB.Text = "" Then
                obm.Text = String.Format("{0:n2}", Qty_Monthly_OB.Text / Plan_Monthly_OB.Text * 100)
            Else
                obm.Text = "0"
            End If

            If Not Qty_Yearly_OB.Text = "" And Not Plan_Yearly_OB.Text = "" Then
                oby.Text = String.Format("{0:n2}", Qty_Yearly_OB.Text / Plan_Yearly_OB.Text * 100)
            Else
                oby.Text = "0"
            End If


            If obd.Text >= 100 Then
                obd.ForeColor = Drawing.Color.White
                obd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf obd.Text > 79 And obd.Text < 100 Then
                obd.ForeColor = Drawing.Color.Black
                obd.BackColor = Drawing.Color.Yellow
            Else
                obd.ForeColor = Drawing.Color.White
                obd.BackColor = Drawing.Color.Red
            End If

            If obm.Text >= 100 Then
                obm.ForeColor = Drawing.Color.White
                obm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf obm.Text > 79 And obm.Text < 100 Then
                obm.ForeColor = Drawing.Color.Black
                obm.BackColor = Drawing.Color.Yellow
            Else
                obm.ForeColor = Drawing.Color.White
                obm.BackColor = Drawing.Color.Red
            End If

            If oby.Text >= 100 Then
                oby.ForeColor = Drawing.Color.White
                oby.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf oby.Text > 79 And oby.Text < 100 Then
                oby.ForeColor = Drawing.Color.Black
                oby.BackColor = Drawing.Color.Yellow
            Else
                oby.ForeColor = Drawing.Color.White
                oby.BackColor = Drawing.Color.Red
            End If

            obd.Text = obd.Text & " %"
            obm.Text = obm.Text & " %"
            oby.Text = oby.Text & " %"

        End If
    End Sub

    Protected Sub DLProduction4_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLProduction4.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim Plan_Daily_CP As Label = CType(e.Item.FindControl("Label1"), Label)
            Dim Plan_Monthly_CP As Label = CType(e.Item.FindControl("Label2"), Label)
            Dim Plan_Yearly_CP As Label = CType(e.Item.FindControl("Label3"), Label)
            Dim Qty_Daily_CP As Label = CType(e.Item.FindControl("Label85"), Label)
            Dim Qty_Monthly_CP As Label = CType(e.Item.FindControl("Label5"), Label)
            Dim Qty_Yearly_CP As Label = CType(e.Item.FindControl("Label6"), Label)
            Dim cgd As Label = CType(e.Item.FindControl("Label26"), Label)
            Dim cgm As Label = CType(e.Item.FindControl("Label27"), Label)
            Dim cgy As Label = CType(e.Item.FindControl("Label28"), Label)


            If Not Qty_Daily_CP.Text = "" And Not Plan_Daily_CP.Text = "" Then
                cgd.Text = String.Format("{0:n2}", Qty_Daily_CP.Text / Plan_Daily_CP.Text * 100)
            Else
                cgd.Text = "0"
            End If

            If Not Qty_Monthly_CP.Text = "" And Not Plan_Monthly_CP.Text = "" Then
                cgm.Text = String.Format("{0:n2}", Qty_Monthly_CP.Text / Plan_Monthly_CP.Text * 100)
            Else
                cgm.Text = "0"
            End If

            If Not Qty_Yearly_CP.Text = "" And Not Plan_Yearly_CP.Text = "" Then
                cgy.Text = String.Format("{0:n2}", Qty_Yearly_CP.Text / Plan_Yearly_CP.Text * 100)
            Else
                cgy.Text = "0"
            End If

            If cgd.Text >= 100 Then
                cgd.ForeColor = Drawing.Color.White
                cgd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgd.Text > 79 And cgd.Text < 100 Then
                cgd.ForeColor = Drawing.Color.Black
                cgd.BackColor = Drawing.Color.Yellow
            Else
                cgd.ForeColor = Drawing.Color.White
                cgd.BackColor = Drawing.Color.Red
            End If

            If cgm.Text >= 100 Then
                cgm.ForeColor = Drawing.Color.White
                cgm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgm.Text > 79 And cgm.Text < 100 Then
                cgm.ForeColor = Drawing.Color.Black
                cgm.BackColor = Drawing.Color.Yellow
            Else
                cgm.ForeColor = Drawing.Color.White
                cgm.BackColor = Drawing.Color.Red
            End If

            If cgy.Text >= 100 Then
                cgy.ForeColor = Drawing.Color.White
                cgy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgy.Text > 79 And cgy.Text < 100 Then
                cgy.ForeColor = Drawing.Color.Black
                cgy.BackColor = Drawing.Color.Yellow
            Else
                cgy.ForeColor = Drawing.Color.White
                cgy.BackColor = Drawing.Color.Red
            End If

            cgd.Text = cgd.Text & " %"
            cgm.Text = cgm.Text & " %"
            cgy.Text = cgy.Text & " %"



            Dim Plan_Daily_OB As Label = CType(e.Item.FindControl("lblPlan_Daily"), Label)
            Dim Plan_Monthly_OB As Label = CType(e.Item.FindControl("lblPlan_Monthly"), Label)
            Dim Plan_Yearly_OB As Label = CType(e.Item.FindControl("lblPlan_Yearly"), Label)
            Dim Qty_Daily_OB As Label = CType(e.Item.FindControl("lblDaily_Actual"), Label)
            Dim Qty_Monthly_OB As Label = CType(e.Item.FindControl("lblMTD_Actual"), Label)
            Dim Qty_Yearly_OB As Label = CType(e.Item.FindControl("lblYTD_Actual"), Label)
            Dim obd As Label = CType(e.Item.FindControl("Label29"), Label)
            Dim obm As Label = CType(e.Item.FindControl("Label30"), Label)
            Dim oby As Label = CType(e.Item.FindControl("Label31"), Label)


            If Not Qty_Daily_OB.Text = "" And Not Plan_Daily_OB.Text = "" Then
                obd.Text = String.Format("{0:n2}", Qty_Daily_OB.Text / Plan_Daily_OB.Text * 100)
            Else
                obd.Text = "0"
            End If

            If Not Qty_Monthly_OB.Text = "" And Not Plan_Monthly_OB.Text = "" Then
                obm.Text = String.Format("{0:n2}", Qty_Monthly_OB.Text / Plan_Monthly_OB.Text * 100)
            Else
                obm.Text = "0"
            End If

            If Not Qty_Yearly_OB.Text = "" And Not Plan_Yearly_OB.Text = "" Then
                oby.Text = String.Format("{0:n2}", Qty_Yearly_OB.Text / Plan_Yearly_OB.Text * 100)
            Else
                oby.Text = "0"
            End If


            If obd.Text >= 100 Then
                obd.ForeColor = Drawing.Color.White
                obd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf obd.Text > 79 And obd.Text < 100 Then
                obd.ForeColor = Drawing.Color.Black
                obd.BackColor = Drawing.Color.Yellow
            Else
                obd.ForeColor = Drawing.Color.White
                obd.BackColor = Drawing.Color.Red
            End If

            If obm.Text >= 100 Then
                obm.ForeColor = Drawing.Color.White
                obm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf obm.Text > 79 And obm.Text < 100 Then
                obm.ForeColor = Drawing.Color.Black
                obm.BackColor = Drawing.Color.Yellow
            Else
                obm.ForeColor = Drawing.Color.White
                obm.BackColor = Drawing.Color.Red
            End If

            If oby.Text >= 100 Then
                oby.ForeColor = Drawing.Color.White
                oby.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf oby.Text > 79 And oby.Text < 100 Then
                oby.ForeColor = Drawing.Color.Black
                oby.BackColor = Drawing.Color.Yellow
            Else
                oby.ForeColor = Drawing.Color.White
                oby.BackColor = Drawing.Color.Red
            End If

            obd.Text = obd.Text & " %"
            obm.Text = obm.Text & " %"
            oby.Text = oby.Text & " %"

        End If
    End Sub

    Protected Sub DLProductionTotal4_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLProductionTotal4.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim Plan_Daily_CP As Label = CType(e.Item.FindControl("Label1"), Label)
            Dim Plan_Monthly_CP As Label = CType(e.Item.FindControl("Label2"), Label)
            Dim Plan_Yearly_CP As Label = CType(e.Item.FindControl("Label3"), Label)
            Dim Qty_Daily_CP As Label = CType(e.Item.FindControl("Label4"), Label)
            Dim Qty_Monthly_CP As Label = CType(e.Item.FindControl("Label5"), Label)
            Dim Qty_Yearly_CP As Label = CType(e.Item.FindControl("Label6"), Label)
            Dim cgd As Label = CType(e.Item.FindControl("Label32"), Label)
            Dim cgm As Label = CType(e.Item.FindControl("Label33"), Label)
            Dim cgy As Label = CType(e.Item.FindControl("Label34"), Label)


            If Not Qty_Daily_CP.Text = "" And Not Plan_Daily_CP.Text = "" Then
                cgd.Text = String.Format("{0:n2}", Qty_Daily_CP.Text / Plan_Daily_CP.Text * 100)
            Else
                cgd.Text = "0"
            End If

            If Not Qty_Monthly_CP.Text = "" And Not Plan_Monthly_CP.Text = "" Then
                cgm.Text = String.Format("{0:n2}", Qty_Monthly_CP.Text / Plan_Monthly_CP.Text * 100)
            Else
                cgm.Text = "0"
            End If

            If Not Qty_Yearly_CP.Text = "" And Not Plan_Yearly_CP.Text = "" Then
                cgy.Text = String.Format("{0:n2}", Qty_Yearly_CP.Text / Plan_Yearly_CP.Text * 100)
            Else
                cgy.Text = "0"
            End If


            If cgd.Text >= 100 Then
                cgd.ForeColor = Drawing.Color.White
                cgd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgd.Text > 79 And cgd.Text < 100 Then
                cgd.ForeColor = Drawing.Color.Black
                cgd.BackColor = Drawing.Color.Yellow
            Else
                cgd.ForeColor = Drawing.Color.White
                cgd.BackColor = Drawing.Color.Red
            End If

            If cgm.Text >= 100 Then
                cgm.ForeColor = Drawing.Color.White
                cgm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgm.Text > 79 And cgm.Text < 100 Then
                cgm.ForeColor = Drawing.Color.Black
                cgm.BackColor = Drawing.Color.Yellow
            Else
                cgm.ForeColor = Drawing.Color.White
                cgm.BackColor = Drawing.Color.Red
            End If

            If cgy.Text >= 100 Then
                cgy.ForeColor = Drawing.Color.White
                cgy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgy.Text > 79 And cgy.Text < 100 Then
                cgy.ForeColor = Drawing.Color.Black
                cgy.BackColor = Drawing.Color.Yellow
            Else
                cgy.ForeColor = Drawing.Color.White
                cgy.BackColor = Drawing.Color.Red
            End If

            cgd.Text = cgd.Text & " %"
            cgm.Text = cgm.Text & " %"
            cgy.Text = cgy.Text & " %"


            Dim Plan_Daily_OB As Label = CType(e.Item.FindControl("lblPlan_Daily"), Label)
            Dim Plan_Monthly_OB As Label = CType(e.Item.FindControl("lblPlan_Monthly"), Label)
            Dim Plan_Yearly_OB As Label = CType(e.Item.FindControl("lblPlan_Yearly"), Label)
            Dim Qty_Daily_OB As Label = CType(e.Item.FindControl("lblDaily_Actual"), Label)
            Dim Qty_Monthly_OB As Label = CType(e.Item.FindControl("lblMTD_Actual"), Label)
            Dim Qty_Yearly_OB As Label = CType(e.Item.FindControl("lblYTD_Actual"), Label)
            Dim obd As Label = CType(e.Item.FindControl("Label35"), Label)
            Dim obm As Label = CType(e.Item.FindControl("Label36"), Label)
            Dim oby As Label = CType(e.Item.FindControl("Label37"), Label)


            If Not Qty_Daily_OB.Text = "" And Not Plan_Daily_OB.Text = "" Then
                obd.Text = String.Format("{0:n2}", Qty_Daily_OB.Text / Plan_Daily_OB.Text * 100)
            Else
                obd.Text = "0"
            End If

            If Not Qty_Monthly_OB.Text = "" And Not Plan_Monthly_OB.Text = "" Then
                obm.Text = String.Format("{0:n2}", Qty_Monthly_OB.Text / Plan_Monthly_OB.Text * 100)
            Else
                obm.Text = "0"
            End If

            If Not Qty_Yearly_OB.Text = "" And Not Plan_Yearly_OB.Text = "" Then
                oby.Text = String.Format("{0:n2}", Qty_Yearly_OB.Text / Plan_Yearly_OB.Text * 100)
            Else
                oby.Text = "0"
            End If


            If obd.Text >= 100 Then
                obd.ForeColor = Drawing.Color.White
                obd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf obd.Text > 79 And obd.Text < 100 Then
                obd.ForeColor = Drawing.Color.Black
                obd.BackColor = Drawing.Color.Yellow
            Else
                obd.ForeColor = Drawing.Color.White
                obd.BackColor = Drawing.Color.Red
            End If

            If obm.Text >= 100 Then
                obm.ForeColor = Drawing.Color.White
                obm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf obm.Text > 79 And obm.Text < 100 Then
                obm.ForeColor = Drawing.Color.Black
                obm.BackColor = Drawing.Color.Yellow
            Else
                obm.ForeColor = Drawing.Color.White
                obm.BackColor = Drawing.Color.Red
            End If

            If oby.Text >= 100 Then
                oby.ForeColor = Drawing.Color.White
                oby.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf oby.Text > 79 And oby.Text < 100 Then
                oby.ForeColor = Drawing.Color.Black
                oby.BackColor = Drawing.Color.Yellow
            Else
                oby.ForeColor = Drawing.Color.White
                oby.BackColor = Drawing.Color.Red
            End If

            obd.Text = obd.Text & " %"
            obm.Text = obm.Text & " %"
            oby.Text = oby.Text & " %"

        End If
    End Sub


    Protected Sub DLProduction5_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLProduction5.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim Plan_Daily_CP As Label = CType(e.Item.FindControl("Label1"), Label)
            Dim Plan_Monthly_CP As Label = CType(e.Item.FindControl("Label2"), Label)
            Dim Plan_Yearly_CP As Label = CType(e.Item.FindControl("Label3"), Label)
            Dim Qty_Daily_CP As Label = CType(e.Item.FindControl("Label85"), Label)
            Dim Qty_Monthly_CP As Label = CType(e.Item.FindControl("Label5"), Label)
            Dim Qty_Yearly_CP As Label = CType(e.Item.FindControl("Label6"), Label)
            Dim cgd As Label = CType(e.Item.FindControl("Label261"), Label)
            Dim cgm As Label = CType(e.Item.FindControl("Label271"), Label)
            Dim cgy As Label = CType(e.Item.FindControl("Label281"), Label)


            If Not Qty_Daily_CP.Text = "" And Not Plan_Daily_CP.Text = "" Then
                cgd.Text = String.Format("{0:n2}", Qty_Daily_CP.Text / Plan_Daily_CP.Text * 100)
            Else
                cgd.Text = "0"
            End If

            If Not Qty_Monthly_CP.Text = "" And Not Plan_Monthly_CP.Text = "" Then
                cgm.Text = String.Format("{0:n2}", Qty_Monthly_CP.Text / Plan_Monthly_CP.Text * 100)
            Else
                cgm.Text = "0"
            End If

            If Not Qty_Yearly_CP.Text = "" And Not Plan_Yearly_CP.Text = "" Then
                cgy.Text = String.Format("{0:n2}", Qty_Yearly_CP.Text / Plan_Yearly_CP.Text * 100)
            Else
                cgy.Text = "0"
            End If

            If cgd.Text >= 100 Then
                cgd.ForeColor = Drawing.Color.White
                cgd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgd.Text > 79 And cgd.Text < 100 Then
                cgd.ForeColor = Drawing.Color.Black
                cgd.BackColor = Drawing.Color.Yellow
            Else
                cgd.ForeColor = Drawing.Color.White
                cgd.BackColor = Drawing.Color.Red
            End If

            If cgm.Text >= 100 Then
                cgm.ForeColor = Drawing.Color.White
                cgm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgm.Text > 79 And cgm.Text < 100 Then
                cgm.ForeColor = Drawing.Color.Black
                cgm.BackColor = Drawing.Color.Yellow
            Else
                cgm.ForeColor = Drawing.Color.White
                cgm.BackColor = Drawing.Color.Red
            End If

            If cgy.Text >= 100 Then
                cgy.ForeColor = Drawing.Color.White
                cgy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgy.Text > 79 And cgy.Text < 100 Then
                cgy.ForeColor = Drawing.Color.Black
                cgy.BackColor = Drawing.Color.Yellow
            Else
                cgy.ForeColor = Drawing.Color.White
                cgy.BackColor = Drawing.Color.Red
            End If

            cgd.Text = cgd.Text & " %"
            cgm.Text = cgm.Text & " %"
            cgy.Text = cgy.Text & " %"



            Dim Plan_Daily_OB As Label = CType(e.Item.FindControl("lblPlan_Daily"), Label)
            Dim Plan_Monthly_OB As Label = CType(e.Item.FindControl("lblPlan_Monthly"), Label)
            Dim Plan_Yearly_OB As Label = CType(e.Item.FindControl("lblPlan_Yearly"), Label)
            Dim Qty_Daily_OB As Label = CType(e.Item.FindControl("lblDaily_Actual"), Label)
            Dim Qty_Monthly_OB As Label = CType(e.Item.FindControl("lblMTD_Actual"), Label)
            Dim Qty_Yearly_OB As Label = CType(e.Item.FindControl("lblYTD_Actual"), Label)
            Dim obd As Label = CType(e.Item.FindControl("Label291"), Label)
            Dim obm As Label = CType(e.Item.FindControl("Label301"), Label)
            Dim oby As Label = CType(e.Item.FindControl("Label311"), Label)


            If Not Qty_Daily_OB.Text = "" And Not Plan_Daily_OB.Text = "" Then
                obd.Text = String.Format("{0:n2}", Qty_Daily_OB.Text / Plan_Daily_OB.Text * 100)
            Else
                obd.Text = "0"
            End If

            If Not Qty_Monthly_OB.Text = "" And Not Plan_Monthly_OB.Text = "" Then
                obm.Text = String.Format("{0:n2}", Qty_Monthly_OB.Text / Plan_Monthly_OB.Text * 100)
            Else
                obm.Text = "0"
            End If

            If Not Qty_Yearly_OB.Text = "" And Not Plan_Yearly_OB.Text = "" Then
                oby.Text = String.Format("{0:n2}", Qty_Yearly_OB.Text / Plan_Yearly_OB.Text * 100)
            Else
                oby.Text = "0"
            End If


            If obd.Text >= 100 Then
                obd.ForeColor = Drawing.Color.White
                obd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf obd.Text > 79 And obd.Text < 100 Then
                obd.ForeColor = Drawing.Color.Black
                obd.BackColor = Drawing.Color.Yellow
            Else
                obd.ForeColor = Drawing.Color.White
                obd.BackColor = Drawing.Color.Red
            End If

            If obm.Text >= 100 Then
                obm.ForeColor = Drawing.Color.White
                obm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf obm.Text > 79 And obm.Text < 100 Then
                obm.ForeColor = Drawing.Color.Black
                obm.BackColor = Drawing.Color.Yellow
            Else
                obm.ForeColor = Drawing.Color.White
                obm.BackColor = Drawing.Color.Red
            End If

            If oby.Text >= 100 Then
                oby.ForeColor = Drawing.Color.White
                oby.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf oby.Text > 79 And oby.Text < 100 Then
                oby.ForeColor = Drawing.Color.Black
                oby.BackColor = Drawing.Color.Yellow
            Else
                oby.ForeColor = Drawing.Color.White
                oby.BackColor = Drawing.Color.Red
            End If

            obd.Text = obd.Text & " %"
            obm.Text = obm.Text & " %"
            oby.Text = oby.Text & " %"

        End If
    End Sub

    Protected Sub DLProductionTotal5_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLProductionTotal5.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim Plan_Daily_CP As Label = CType(e.Item.FindControl("Label1"), Label)
            Dim Plan_Monthly_CP As Label = CType(e.Item.FindControl("Label2"), Label)
            Dim Plan_Yearly_CP As Label = CType(e.Item.FindControl("Label3"), Label)
            Dim Qty_Daily_CP As Label = CType(e.Item.FindControl("Label4"), Label)
            Dim Qty_Monthly_CP As Label = CType(e.Item.FindControl("Label5"), Label)
            Dim Qty_Yearly_CP As Label = CType(e.Item.FindControl("Label6"), Label)
            Dim cgd As Label = CType(e.Item.FindControl("Label321"), Label)
            Dim cgm As Label = CType(e.Item.FindControl("Label331"), Label)
            Dim cgy As Label = CType(e.Item.FindControl("Label341"), Label)


            If Not Qty_Daily_CP.Text = "" And Not Plan_Daily_CP.Text = "" Then
                cgd.Text = String.Format("{0:n2}", Qty_Daily_CP.Text / Plan_Daily_CP.Text * 100)
            Else
                cgd.Text = "0"
            End If

            If Not Qty_Monthly_CP.Text = "" And Not Plan_Monthly_CP.Text = "" Then
                cgm.Text = String.Format("{0:n2}", Qty_Monthly_CP.Text / Plan_Monthly_CP.Text * 100)
            Else
                cgm.Text = "0"
            End If

            If Not Qty_Yearly_CP.Text = "" And Not Plan_Yearly_CP.Text = "" Then
                cgy.Text = String.Format("{0:n2}", Qty_Yearly_CP.Text / Plan_Yearly_CP.Text * 100)
            Else
                cgy.Text = "0"
            End If


            If cgd.Text >= 100 Then
                cgd.ForeColor = Drawing.Color.White
                cgd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgd.Text > 79 And cgd.Text < 100 Then
                cgd.ForeColor = Drawing.Color.Black
                cgd.BackColor = Drawing.Color.Yellow
            Else
                cgd.ForeColor = Drawing.Color.White
                cgd.BackColor = Drawing.Color.Red
            End If

            If cgm.Text >= 100 Then
                cgm.ForeColor = Drawing.Color.White
                cgm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgm.Text > 79 And cgm.Text < 100 Then
                cgm.ForeColor = Drawing.Color.Black
                cgm.BackColor = Drawing.Color.Yellow
            Else
                cgm.ForeColor = Drawing.Color.White
                cgm.BackColor = Drawing.Color.Red
            End If

            If cgy.Text >= 100 Then
                cgy.ForeColor = Drawing.Color.White
                cgy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgy.Text > 79 And cgy.Text < 100 Then
                cgy.ForeColor = Drawing.Color.Black
                cgy.BackColor = Drawing.Color.Yellow
            Else
                cgy.ForeColor = Drawing.Color.White
                cgy.BackColor = Drawing.Color.Red
            End If

            cgd.Text = cgd.Text & " %"
            cgm.Text = cgm.Text & " %"
            cgy.Text = cgy.Text & " %"


            Dim Plan_Daily_OB As Label = CType(e.Item.FindControl("lblPlan_Daily"), Label)
            Dim Plan_Monthly_OB As Label = CType(e.Item.FindControl("lblPlan_Monthly"), Label)
            Dim Plan_Yearly_OB As Label = CType(e.Item.FindControl("lblPlan_Yearly"), Label)
            Dim Qty_Daily_OB As Label = CType(e.Item.FindControl("lblDaily_Actual"), Label)
            Dim Qty_Monthly_OB As Label = CType(e.Item.FindControl("lblMTD_Actual"), Label)
            Dim Qty_Yearly_OB As Label = CType(e.Item.FindControl("lblYTD_Actual"), Label)
            Dim obd As Label = CType(e.Item.FindControl("Label351"), Label)
            Dim obm As Label = CType(e.Item.FindControl("Label361"), Label)
            Dim oby As Label = CType(e.Item.FindControl("Label371"), Label)


            If Not Qty_Daily_OB.Text = "" And Not Plan_Daily_OB.Text = "" Then
                obd.Text = String.Format("{0:n2}", Qty_Daily_OB.Text / Plan_Daily_OB.Text * 100)
            Else
                obd.Text = "0"
            End If

            If Not Qty_Monthly_OB.Text = "" And Not Plan_Monthly_OB.Text = "" Then
                obm.Text = String.Format("{0:n2}", Qty_Monthly_OB.Text / Plan_Monthly_OB.Text * 100)
            Else
                obm.Text = "0"
            End If

            If Not Qty_Yearly_OB.Text = "" And Not Plan_Yearly_OB.Text = "" Then
                oby.Text = String.Format("{0:n2}", Qty_Yearly_OB.Text / Plan_Yearly_OB.Text * 100)
            Else
                oby.Text = "0"
            End If


            If obd.Text >= 100 Then
                obd.ForeColor = Drawing.Color.White
                obd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf obd.Text > 79 And obd.Text < 100 Then
                obd.ForeColor = Drawing.Color.Black
                obd.BackColor = Drawing.Color.Yellow
            Else
                obd.ForeColor = Drawing.Color.White
                obd.BackColor = Drawing.Color.Red
            End If

            If obm.Text >= 100 Then
                obm.ForeColor = Drawing.Color.White
                obm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf obm.Text > 79 And obm.Text < 100 Then
                obm.ForeColor = Drawing.Color.Black
                obm.BackColor = Drawing.Color.Yellow
            Else
                obm.ForeColor = Drawing.Color.White
                obm.BackColor = Drawing.Color.Red
            End If

            If oby.Text >= 100 Then
                oby.ForeColor = Drawing.Color.White
                oby.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf oby.Text > 79 And oby.Text < 100 Then
                oby.ForeColor = Drawing.Color.Black
                oby.BackColor = Drawing.Color.Yellow
            Else
                oby.ForeColor = Drawing.Color.White
                oby.BackColor = Drawing.Color.Red
            End If

            obd.Text = obd.Text & " %"
            obm.Text = obm.Text & " %"
            oby.Text = oby.Text & " %"

        End If
    End Sub

    Protected Sub DLProductionTotalAll_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLProductionTotalAll.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim Plan_Daily_CP As Label = CType(e.Item.FindControl("Label1"), Label)
            Dim Plan_Monthly_CP As Label = CType(e.Item.FindControl("Label2"), Label)
            Dim Plan_Yearly_CP As Label = CType(e.Item.FindControl("Label3"), Label)
            Dim Qty_Daily_CP As Label = CType(e.Item.FindControl("Label4"), Label)
            Dim Qty_Monthly_CP As Label = CType(e.Item.FindControl("Label5"), Label)
            Dim Qty_Yearly_CP As Label = CType(e.Item.FindControl("Label6"), Label)
            Dim cgd As Label = CType(e.Item.FindControl("Label32"), Label)
            Dim cgm As Label = CType(e.Item.FindControl("Label33"), Label)
            Dim cgy As Label = CType(e.Item.FindControl("Label34"), Label)


            If Not Qty_Daily_CP.Text = "" And Not Plan_Daily_CP.Text = "" Then
                cgd.Text = String.Format("{0:n2}", Qty_Daily_CP.Text / Plan_Daily_CP.Text * 100)
            Else
                cgd.Text = "0"
            End If

            If Not Qty_Monthly_CP.Text = "" And Not Plan_Monthly_CP.Text = "" Then
                cgm.Text = String.Format("{0:n2}", Qty_Monthly_CP.Text / Plan_Monthly_CP.Text * 100)
            Else
                cgm.Text = "0"
            End If

            If Not Qty_Yearly_CP.Text = "" And Not Plan_Yearly_CP.Text = "" Then
                cgy.Text = String.Format("{0:n2}", Qty_Yearly_CP.Text / Plan_Yearly_CP.Text * 100)
            Else
                cgy.Text = "0"
            End If


            If cgd.Text >= 100 Then
                cgd.ForeColor = Drawing.Color.White
                cgd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgd.Text > 79 And cgd.Text < 100 Then
                cgd.ForeColor = Drawing.Color.Black
                cgd.BackColor = Drawing.Color.Yellow
            Else
                cgd.ForeColor = Drawing.Color.White
                cgd.BackColor = Drawing.Color.Red
            End If

            If cgm.Text >= 100 Then
                cgm.ForeColor = Drawing.Color.White
                cgm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgm.Text > 79 And cgm.Text < 100 Then
                cgm.ForeColor = Drawing.Color.Black
                cgm.BackColor = Drawing.Color.Yellow
            Else
                cgm.ForeColor = Drawing.Color.White
                cgm.BackColor = Drawing.Color.Red
            End If

            If cgy.Text >= 100 Then
                cgy.ForeColor = Drawing.Color.White
                cgy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf cgy.Text > 79 And cgy.Text < 100 Then
                cgy.ForeColor = Drawing.Color.Black
                cgy.BackColor = Drawing.Color.Yellow
            Else
                cgy.ForeColor = Drawing.Color.White
                cgy.BackColor = Drawing.Color.Red
            End If

            cgd.Text = cgd.Text & " %"
            cgm.Text = cgm.Text & " %"
            cgy.Text = cgy.Text & " %"


            Dim Plan_Daily_OB As Label = CType(e.Item.FindControl("lblPlan_Daily"), Label)
            Dim Plan_Monthly_OB As Label = CType(e.Item.FindControl("lblPlan_Monthly"), Label)
            Dim Plan_Yearly_OB As Label = CType(e.Item.FindControl("lblPlan_Yearly"), Label)
            Dim Qty_Daily_OB As Label = CType(e.Item.FindControl("lblDaily_Actual"), Label)
            Dim Qty_Monthly_OB As Label = CType(e.Item.FindControl("lblMTD_Actual"), Label)
            Dim Qty_Yearly_OB As Label = CType(e.Item.FindControl("lblYTD_Actual"), Label)
            Dim obd As Label = CType(e.Item.FindControl("Label35"), Label)
            Dim obm As Label = CType(e.Item.FindControl("Label36"), Label)
            Dim oby As Label = CType(e.Item.FindControl("Label37"), Label)


            If Not Qty_Daily_OB.Text = "" And Not Plan_Daily_OB.Text = "" Then
                obd.Text = String.Format("{0:n2}", Qty_Daily_OB.Text / Plan_Daily_OB.Text * 100)
            Else
                obd.Text = "0"
            End If

            If Not Qty_Monthly_OB.Text = "" And Not Plan_Monthly_OB.Text = "" Then
                obm.Text = String.Format("{0:n2}", Qty_Monthly_OB.Text / Plan_Monthly_OB.Text * 100)
            Else
                obm.Text = "0"
            End If

            If Not Qty_Yearly_OB.Text = "" And Not Plan_Yearly_OB.Text = "" Then
                oby.Text = String.Format("{0:n2}", Qty_Yearly_OB.Text / Plan_Yearly_OB.Text * 100)
            Else
                oby.Text = "0"
            End If


            If obd.Text >= 100 Then
                obd.ForeColor = Drawing.Color.White
                obd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf obd.Text > 79 And obd.Text < 100 Then
                obd.ForeColor = Drawing.Color.Black
                obd.BackColor = Drawing.Color.Yellow
            Else
                obd.ForeColor = Drawing.Color.White
                obd.BackColor = Drawing.Color.Red
            End If

            If obm.Text >= 100 Then
                obm.ForeColor = Drawing.Color.White
                obm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf obm.Text > 79 And obm.Text < 100 Then
                obm.ForeColor = Drawing.Color.Black
                obm.BackColor = Drawing.Color.Yellow
            Else
                obm.ForeColor = Drawing.Color.White
                obm.BackColor = Drawing.Color.Red
            End If

            If oby.Text >= 100 Then
                oby.ForeColor = Drawing.Color.White
                oby.BackColor = Drawing.Color.FromArgb(34, 150, 14)
            ElseIf oby.Text > 79 And oby.Text < 100 Then
                oby.ForeColor = Drawing.Color.Black
                oby.BackColor = Drawing.Color.Yellow
            Else
                oby.ForeColor = Drawing.Color.White
                oby.BackColor = Drawing.Color.Red
            End If

            obd.Text = obd.Text & " %"
            obm.Text = obm.Text & " %"
            oby.Text = oby.Text & " %"

        End If
    End Sub

    Protected Sub DLCrushing_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLCrushing.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim Plan_Daily_S1 As Label = CType(e.Item.FindControl("Label1"), Label)
            Dim Qty_Daily_S1 As Label = CType(e.Item.FindControl("Label2"), Label)

            Dim Plan_Daily_S2 As Label = CType(e.Item.FindControl("Label8"), Label)
            Dim Qty_Daily_S2 As Label = CType(e.Item.FindControl("Label9"), Label)

            Dim Total_Plan_Daily As Label = CType(e.Item.FindControl("Label13"), Label)
            Dim Total_Qty_Daily As Label = CType(e.Item.FindControl("Label14"), Label)

            Dim Total_Plan_Monthly As Label = CType(e.Item.FindControl("Label72"), Label)
            Dim Total_Qty_Monthly As Label = CType(e.Item.FindControl("Label3"), Label)

            Dim Total_Plan_Yearly As Label = CType(e.Item.FindControl("Label74"), Label)
            Dim Total_Qty_Yearly As Label = CType(e.Item.FindControl("Label11"), Label)


            Dim cd1 As Label = CType(e.Item.FindControl("Label44"), Label)
            Dim cd2 As Label = CType(e.Item.FindControl("Label45"), Label)
            Dim totalCD As Label = CType(e.Item.FindControl("Label46"), Label)
            Dim totalCM As Label = CType(e.Item.FindControl("Label10"), Label)
            Dim totalCY As Label = CType(e.Item.FindControl("Label12"), Label)


            If Qty_Daily_S1.Text = "" Or Qty_Daily_S1.Text = "0.00" Or
                Plan_Daily_S1.Text = "" Or Plan_Daily_S1.Text = "0.00" Then
                cd1.Text = "0"
            Else
                cd1.Text = String.Format("{0:n2}", Qty_Daily_S1.Text / Plan_Daily_S1.Text * 100)
            End If

            If Qty_Daily_S2.Text = "" Or Qty_Daily_S2.Text = "0.00" Or
                Plan_Daily_S2.Text = "" Or Plan_Daily_S2.Text = "0.00" Then
                cd2.Text = "0"
            Else
                cd2.Text = String.Format("{0:n2}", Qty_Daily_S2.Text / Plan_Daily_S2.Text * 100)
            End If

            If Total_Qty_Daily.Text = "" Or Total_Qty_Daily.Text = "0.00" Or
                Total_Plan_Daily.Text = "" Or Total_Plan_Daily.Text = "0.00" Then
                totalCD.Text = "0"
            Else
                totalCD.Text = String.Format("{0:n2}", Total_Qty_Daily.Text / Total_Plan_Daily.Text * 100)
            End If

            If Total_Qty_Monthly.Text = "" Or Total_Qty_Monthly.Text = "0.00" Or
                Total_Plan_Monthly.Text = "" Or Total_Plan_Monthly.Text = "0.00" Then
                totalCM.Text = "0"
            Else
                totalCM.Text = String.Format("{0:n2}", Total_Qty_Monthly.Text / Total_Plan_Monthly.Text * 100)
            End If

            If Total_Qty_Yearly.Text = "" Or Total_Plan_Yearly.Text = "" Or
                Total_Qty_Yearly.Text = "0.00" Or Total_Plan_Yearly.Text = "0.00" Then
                totalCY.Text = "0"
            Else
                totalCY.Text = String.Format("{0:n2}", Total_Qty_Yearly.Text / Total_Plan_Yearly.Text * 100)
            End If

            If cd1.Text >= 100 Then
                cd1.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                cd1.ForeColor = Drawing.Color.White
            ElseIf cd1.Text > 79 And cd1.Text < 100 Then
                cd1.BackColor = Drawing.Color.Yellow
                cd1.ForeColor = Drawing.Color.Black
            Else
                cd1.BackColor = Drawing.Color.Red
                cd1.ForeColor = Drawing.Color.White
            End If

            If cd2.Text >= 100 Then
                cd2.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                cd2.ForeColor = Drawing.Color.White
            ElseIf cd2.Text > 79 And cd2.Text < 100 Then
                cd2.BackColor = Drawing.Color.Yellow
                cd2.ForeColor = Drawing.Color.Black
            Else
                cd2.BackColor = Drawing.Color.Red
                cd2.ForeColor = Drawing.Color.White
            End If

            If totalCD.Text >= 100 Then
                totalCD.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                totalCD.ForeColor = Drawing.Color.White
            ElseIf totalCD.Text > 79 And totalCD.Text < 100 Then
                totalCD.BackColor = Drawing.Color.Yellow
                totalCD.ForeColor = Drawing.Color.Black
            Else
                totalCD.BackColor = Drawing.Color.Red
                totalCD.ForeColor = Drawing.Color.White
            End If

            If totalCM.Text >= 100 Then
                totalCM.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                totalCM.ForeColor = Drawing.Color.White
            ElseIf totalCM.Text > 79 And totalCM.Text < 100 Then
                totalCM.BackColor = Drawing.Color.Yellow
                totalCM.ForeColor = Drawing.Color.Black
            Else
                totalCM.BackColor = Drawing.Color.Red
                totalCM.ForeColor = Drawing.Color.White
            End If

            If totalCY.Text >= 100 Then
                totalCY.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                totalCY.ForeColor = Drawing.Color.White
            ElseIf totalCY.Text > 79 And totalCY.Text < 100 Then
                totalCY.BackColor = Drawing.Color.Yellow
                totalCY.ForeColor = Drawing.Color.Black
            Else
                totalCY.BackColor = Drawing.Color.Red
                totalCY.ForeColor = Drawing.Color.White
            End If


            cd1.Text = cd1.Text & " %"
            cd2.Text = cd2.Text & " %"
            totalCD.Text = totalCD.Text & " %"
            totalCM.Text = totalCM.Text & " %"
            totalCY.Text = totalCY.Text & " %"
        End If
    End Sub

    Protected Sub DLCrushingTotal_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLCrushingTotal.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim Plan_Daily As Label = CType(e.Item.FindControl("Label1"), Label)
            Dim Qty_Daily As Label = CType(e.Item.FindControl("Label2"), Label)
            Dim Plan_Monthly As Label = CType(e.Item.FindControl("Label15"), Label)
            Dim Qty_Monthly As Label = CType(e.Item.FindControl("Label3"), Label)
            Dim Plan_Yearly As Label = CType(e.Item.FindControl("Label16"), Label)
            Dim Qty_Yearly As Label = CType(e.Item.FindControl("Label11"), Label)

            Dim cd As Label = CType(e.Item.FindControl("Label47"), Label)
            Dim cm As Label = CType(e.Item.FindControl("Label73"), Label)
            Dim cy As Label = CType(e.Item.FindControl("Label75"), Label)


            If Not Qty_Daily.Text = "" And Not Plan_Daily.Text = "" Then
                cd.Text = String.Format("{0:n2}", Qty_Daily.Text / Plan_Daily.Text * 100)
            Else
                cd.Text = "0"
            End If

            If Not Qty_Monthly.Text = "" And Not Plan_Monthly.Text = "" Then
                cm.Text = String.Format("{0:n2}", Qty_Monthly.Text / Plan_Monthly.Text * 100)
            Else
                cm.Text = "0"
            End If

            If Not Qty_Yearly.Text = "" And Not Plan_Yearly.Text = "" Then
                cy.Text = String.Format("{0:n2}", Qty_Yearly.Text / Plan_Yearly.Text * 100)
            Else
                cy.Text = "0"
            End If


            If cd.Text >= 100 Then
                cd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                cd.ForeColor = Drawing.Color.White
            ElseIf cd.Text > 79 And cd.Text < 100 Then
                cd.BackColor = Drawing.Color.Yellow
                cd.ForeColor = Drawing.Color.Black
            Else
                cd.BackColor = Drawing.Color.Red
                cd.ForeColor = Drawing.Color.White
            End If

            If cm.Text >= 100 Then
                cm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                cm.ForeColor = Drawing.Color.White
            ElseIf cm.Text > 79 And cm.Text < 100 Then
                cm.BackColor = Drawing.Color.Yellow
                cm.ForeColor = Drawing.Color.Black
            Else
                cm.BackColor = Drawing.Color.Red
                cm.ForeColor = Drawing.Color.White
            End If

            If cy.Text >= 100 Then
                cy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                cy.ForeColor = Drawing.Color.White
            ElseIf cy.Text > 79 And cy.Text < 100 Then
                cy.BackColor = Drawing.Color.Yellow
                cy.ForeColor = Drawing.Color.Black
            Else
                cy.BackColor = Drawing.Color.Red
                cy.ForeColor = Drawing.Color.White
            End If


            cd.Text = cd.Text & " %"
            cm.Text = cm.Text & " %"
            cy.Text = cy.Text & " %"
        End If
    End Sub

    Protected Sub DLLogistics_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLLogistics.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim Plan_Daily_TRUS As Label = CType(e.Item.FindControl("lblTRKD"), Label)
            Dim Plan_Monthly_TRUS As Label = CType(e.Item.FindControl("lblTRKM"), Label)
            Dim Plan_Yearly_TRUS As Label = CType(e.Item.FindControl("lblTRKY"), Label)
            Dim Qty_Daily As Label = CType(e.Item.FindControl("lblDaily"), Label)
            Dim Qty_Monthly As Label = CType(e.Item.FindControl("lblMonthly"), Label)
            Dim Qty_Yearly As Label = CType(e.Item.FindControl("lblYearly"), Label)
            Dim cld As Label = CType(e.Item.FindControl("Label38"), Label)
            Dim clm As Label = CType(e.Item.FindControl("Label39"), Label)
            Dim cly As Label = CType(e.Item.FindControl("Label40"), Label)

            Try
                If CInt(Plan_Daily_TRUS.Text) > 0 Then
                    cld.Text = String.Format("{0:n2}", Qty_Daily.Text / Plan_Daily_TRUS.Text * 100)
                Else
                    cld.Text = "0"
                End If
            Catch ex As Exception
                cld.Text = "0"
            End Try

            Try
                If CInt(Plan_Monthly_TRUS.Text) > 0 Then
                    clm.Text = String.Format("{0:n2}", Qty_Monthly.Text / Plan_Monthly_TRUS.Text * 100)
                Else
                    clm.Text = "0"
                End If
            Catch ex As Exception
                clm.Text = "0"
            End Try

            Try
                If CInt(Plan_Yearly_TRUS.Text) > 0 Then
                    cly.Text = String.Format("{0:n2}", Qty_Yearly.Text / Plan_Yearly_TRUS.Text * 100)
                Else
                    cly.Text = "0"
                End If
            Catch ex As Exception
                cly.Text = "0"
            End Try


            If cld.Text >= 100 Then
                cld.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                cld.ForeColor = Drawing.Color.White
            ElseIf cld.Text > 79 And cld.Text < 100 Then
                cld.BackColor = Drawing.Color.Yellow
                cld.ForeColor = Drawing.Color.Black
            Else
                cld.BackColor = Drawing.Color.Red
                cld.ForeColor = Drawing.Color.White
            End If

            If clm.Text >= 100 Then
                clm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                clm.ForeColor = Drawing.Color.White
            ElseIf clm.Text > 79 And clm.Text < 100 Then
                clm.BackColor = Drawing.Color.Yellow
                clm.ForeColor = Drawing.Color.Black
            Else
                clm.BackColor = Drawing.Color.Red
                clm.ForeColor = Drawing.Color.White
            End If

            If cly.Text >= 100 Then
                cly.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                cly.ForeColor = Drawing.Color.White
            ElseIf cly.Text > 79 And cly.Text < 100 Then
                cly.BackColor = Drawing.Color.Yellow
                cly.ForeColor = Drawing.Color.Black
            Else
                cly.BackColor = Drawing.Color.Red
                cly.ForeColor = Drawing.Color.White
            End If

            cld.Text = cld.Text & " %"
            clm.Text = clm.Text & " %"
            cly.Text = cly.Text & " %"
        End If
    End Sub

    Protected Sub DLSales_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLSales.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim PLAN_DAILY As Label = CType(e.Item.FindControl("lblPB_Daily"), Label)
            Dim PLAN_MONTHLY As Label = CType(e.Item.FindControl("lblPB_Monthly"), Label)
            Dim PLAN_YEARLY As Label = CType(e.Item.FindControl("lblPY_Monthly"), Label)
            Dim QTY_DAILY As Label = CType(e.Item.FindControl("lblDaily"), Label)
            Dim QTY_MONTHLY As Label = CType(e.Item.FindControl("lblMonthly"), Label)
            Dim QTY_YEARLY As Label = CType(e.Item.FindControl("lblYearly"), Label)
            Dim sd As Label = CType(e.Item.FindControl("Label41"), Label)
            Dim sm As Label = CType(e.Item.FindControl("Label42"), Label)
            Dim sy As Label = CType(e.Item.FindControl("Label43"), Label)


            Try
                If CInt(PLAN_DAILY.Text) > 0 Then
                    sd.Text = String.Format("{0:n2}", QTY_DAILY.Text / PLAN_DAILY.Text * 100)
                Else
                    sd.Text = "0"
                End If
            Catch ex As Exception
                sd.Text = "0"
            End Try

            Try
                If CInt(PLAN_MONTHLY.Text) > 0 Then
                    sm.Text = String.Format("{0:n2}", QTY_MONTHLY.Text / PLAN_MONTHLY.Text * 100)
                Else
                    sm.Text = "0"
                End If
            Catch ex As Exception
                sm.Text = "0"
            End Try

            Try
                If CInt(PLAN_YEARLY.Text) > 0 Then
                    sy.Text = String.Format("{0:n2}", QTY_YEARLY.Text / PLAN_YEARLY.Text * 100)
                Else
                    sy.Text = "0"
                End If
            Catch ex As Exception
                sy.Text = "0"
            End Try


            If sd.Text >= 100 Then
                sd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sd.ForeColor = Drawing.Color.White
            ElseIf sd.Text > 79 And sd.Text < 100 Then
                sd.BackColor = Drawing.Color.Yellow
                sd.ForeColor = Drawing.Color.Black
            Else
                sd.ForeColor = Drawing.Color.White
                sd.BackColor = Drawing.Color.Red
            End If

            If sm.Text >= 100 Then
                sm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sm.ForeColor = Drawing.Color.White
            ElseIf sm.Text > 79 And sm.Text < 100 Then
                sm.BackColor = Drawing.Color.Yellow
                sm.ForeColor = Drawing.Color.Black
            Else
                sm.ForeColor = Drawing.Color.White
                sm.BackColor = Drawing.Color.Red
            End If

            If sy.Text >= 100 Then
                sy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sy.ForeColor = Drawing.Color.White
            ElseIf sy.Text > 79 And sy.Text < 100 Then
                sy.BackColor = Drawing.Color.Yellow
                sy.ForeColor = Drawing.Color.Black
            Else
                sy.BackColor = Drawing.Color.Red
                sy.ForeColor = Drawing.Color.White
            End If

            sd.Text = sd.Text & " %"
            sm.Text = sm.Text & " %"
            sy.Text = sy.Text & " %"
        End If
    End Sub

    Private Sub DLSalesAll_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLSalesAll.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim PLAN_DAILY As Label = CType(e.Item.FindControl("lblPB_Daily"), Label)
            Dim PLAN_MONTHLY As Label = CType(e.Item.FindControl("lblPB_Monthly"), Label)
            Dim PLAN_YEARLY As Label = CType(e.Item.FindControl("lblPY_Monthly"), Label)
            Dim QTY_DAILY As Label = CType(e.Item.FindControl("lblDaily"), Label)
            Dim QTY_MONTHLY As Label = CType(e.Item.FindControl("lblMonthly"), Label)
            Dim QTY_YEARLY As Label = CType(e.Item.FindControl("lblYearly"), Label)
            Dim sd As Label = CType(e.Item.FindControl("Label41"), Label)
            Dim sm As Label = CType(e.Item.FindControl("Label42"), Label)
            Dim sy As Label = CType(e.Item.FindControl("Label43"), Label)


            Try
                If CInt(PLAN_DAILY.Text) > 0 Then
                    sd.Text = String.Format("{0:n2}", QTY_DAILY.Text / PLAN_DAILY.Text * 100)
                Else
                    sd.Text = "0"
                End If
            Catch ex As Exception
                sd.Text = "0"
            End Try

            Try
                If CInt(PLAN_MONTHLY.Text) > 0 Then
                    sm.Text = String.Format("{0:n2}", QTY_MONTHLY.Text / PLAN_MONTHLY.Text * 100)
                Else
                    sm.Text = "0"
                End If
            Catch ex As Exception
                sm.Text = "0"
            End Try

            Try
                If CInt(PLAN_YEARLY.Text) > 0 Then
                    sy.Text = String.Format("{0:n2}", QTY_YEARLY.Text / PLAN_YEARLY.Text * 100)
                Else
                    sy.Text = "0"
                End If
            Catch ex As Exception
                sy.Text = "0"
            End Try


            If sd.Text >= 100 Then
                sd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sd.ForeColor = Drawing.Color.White
            ElseIf sd.Text > 79 And sd.Text < 100 Then
                sd.BackColor = Drawing.Color.Yellow
                sd.ForeColor = Drawing.Color.Black
            Else
                sd.ForeColor = Drawing.Color.White
                sd.BackColor = Drawing.Color.Red
            End If

            If sm.Text >= 100 Then
                sm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sm.ForeColor = Drawing.Color.White
            ElseIf sm.Text > 79 And sm.Text < 100 Then
                sm.BackColor = Drawing.Color.Yellow
                sm.ForeColor = Drawing.Color.Black
            Else
                sm.ForeColor = Drawing.Color.White
                sm.BackColor = Drawing.Color.Red
            End If

            If sy.Text >= 100 Then
                sy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sy.ForeColor = Drawing.Color.White
            ElseIf sy.Text > 79 And sy.Text < 100 Then
                sy.BackColor = Drawing.Color.Yellow
                sy.ForeColor = Drawing.Color.Black
            Else
                sy.ForeColor = Drawing.Color.White
                sy.BackColor = Drawing.Color.Red
            End If

            sd.Text = sd.Text & " %"
            sm.Text = sm.Text & " %"
            sy.Text = sy.Text & " %"
        End If
    End Sub

    Protected Sub DLSales4_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLSales4.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim PLAN_DAILY As Label = CType(e.Item.FindControl("lblPB_Daily"), Label)
            Dim PLAN_MONTHLY As Label = CType(e.Item.FindControl("lblPB_Monthly"), Label)
            Dim PLAN_YEARLY As Label = CType(e.Item.FindControl("lblPY_Monthly"), Label)
            Dim QTY_DAILY As Label = CType(e.Item.FindControl("lblDaily"), Label)
            Dim QTY_MONTHLY As Label = CType(e.Item.FindControl("lblMonthly"), Label)
            Dim QTY_YEARLY As Label = CType(e.Item.FindControl("lblYearly"), Label)
            Dim sd As Label = CType(e.Item.FindControl("Label41"), Label)
            Dim sm As Label = CType(e.Item.FindControl("Label42"), Label)
            Dim sy As Label = CType(e.Item.FindControl("Label43"), Label)

            Try
                If CInt(PLAN_DAILY.Text) > 0 Then
                    sd.Text = String.Format("{0:n2}", QTY_DAILY.Text / PLAN_DAILY.Text * 100)
                Else
                    sd.Text = "0"
                End If
            Catch ex As Exception
                sd.Text = "0"
            End Try

            Try
                If CInt(PLAN_MONTHLY.Text) > 0 Then
                    sm.Text = String.Format("{0:n2}", QTY_MONTHLY.Text / PLAN_MONTHLY.Text * 100)
                Else
                    sm.Text = "0"
                End If
            Catch ex As Exception
                sm.Text = "0"
            End Try

            Try
                If CInt(PLAN_YEARLY.Text) > 0 Then
                    sy.Text = String.Format("{0:n2}", QTY_YEARLY.Text / PLAN_YEARLY.Text * 100)
                Else
                    sy.Text = "0"
                End If
            Catch ex As Exception
                sy.Text = "0"
            End Try

            If sd.Text >= 100 Then
                sd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sd.ForeColor = Drawing.Color.White
            ElseIf sd.Text > 79 And sd.Text < 100 Then
                sd.BackColor = Drawing.Color.Yellow
                sd.ForeColor = Drawing.Color.Black
            Else
                sd.ForeColor = Drawing.Color.White
                sd.BackColor = Drawing.Color.Red
            End If

            If sm.Text >= 100 Then
                sm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sm.ForeColor = Drawing.Color.White
            ElseIf sm.Text > 79 And sm.Text < 100 Then
                sm.BackColor = Drawing.Color.Yellow
                sm.ForeColor = Drawing.Color.Black
            Else
                sm.ForeColor = Drawing.Color.White
                sm.BackColor = Drawing.Color.Red
            End If

            If sy.Text >= 100 Then
                sy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sy.ForeColor = Drawing.Color.White
            ElseIf sy.Text > 79 And sy.Text < 100 Then
                sy.BackColor = Drawing.Color.Yellow
                sy.ForeColor = Drawing.Color.Black
            Else
                sy.ForeColor = Drawing.Color.White
                sy.BackColor = Drawing.Color.Red
            End If

            sd.Text = sd.Text & " %"
            sm.Text = sm.Text & " %"
            sy.Text = sy.Text & " %"
        End If
    End Sub

    Protected Sub DLSales5_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLSales5.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim PLAN_DAILY As Label = CType(e.Item.FindControl("lblPB_Daily"), Label)
            Dim PLAN_MONTHLY As Label = CType(e.Item.FindControl("lblPB_Monthly"), Label)
            Dim PLAN_YEARLY As Label = CType(e.Item.FindControl("lblPY_Monthly"), Label)
            Dim QTY_DAILY As Label = CType(e.Item.FindControl("lblDaily"), Label)
            Dim QTY_MONTHLY As Label = CType(e.Item.FindControl("lblMonthly"), Label)
            Dim QTY_YEARLY As Label = CType(e.Item.FindControl("lblYearly"), Label)
            Dim sd As Label = CType(e.Item.FindControl("Label41"), Label)
            Dim sm As Label = CType(e.Item.FindControl("Label42"), Label)
            Dim sy As Label = CType(e.Item.FindControl("Label43"), Label)

            Try
                If CInt(PLAN_DAILY.Text) > 0 Then
                    sd.Text = String.Format("{0:n2}", QTY_DAILY.Text / PLAN_DAILY.Text * 100)
                Else
                    sd.Text = "0"
                End If
            Catch ex As Exception
                sd.Text = "0"
            End Try

            Try
                If CInt(PLAN_MONTHLY.Text) > 0 Then
                    sm.Text = String.Format("{0:n2}", QTY_MONTHLY.Text / PLAN_MONTHLY.Text * 100)
                Else
                    sm.Text = "0"
                End If
            Catch ex As Exception
                sm.Text = "0"
            End Try

            Try
                If CInt(PLAN_YEARLY.Text) > 0 Then
                    sy.Text = String.Format("{0:n2}", QTY_YEARLY.Text / PLAN_YEARLY.Text * 100)
                Else
                    sy.Text = "0"
                End If
            Catch ex As Exception
                sy.Text = "0"
            End Try

            If sd.Text >= 100 Then
                sd.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sd.ForeColor = Drawing.Color.White
            ElseIf sd.Text > 79 And sd.Text < 100 Then
                sd.BackColor = Drawing.Color.Yellow
                sd.ForeColor = Drawing.Color.Black
            Else
                sd.ForeColor = Drawing.Color.White
                sd.BackColor = Drawing.Color.Red
            End If

            If sm.Text >= 100 Then
                sm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sm.ForeColor = Drawing.Color.White
            ElseIf sm.Text > 79 And sm.Text < 100 Then
                sm.BackColor = Drawing.Color.Yellow
                sm.ForeColor = Drawing.Color.Black
            Else
                sm.ForeColor = Drawing.Color.White
                sm.BackColor = Drawing.Color.Red
            End If

            If sy.Text >= 100 Then
                sy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sy.ForeColor = Drawing.Color.White
            ElseIf sy.Text > 79 And sy.Text < 100 Then
                sy.BackColor = Drawing.Color.Yellow
                sy.ForeColor = Drawing.Color.Black
            Else
                sy.ForeColor = Drawing.Color.White
                sy.BackColor = Drawing.Color.Red
            End If

            sd.Text = sd.Text & " %"
            sm.Text = sm.Text & " %"
            sy.Text = sy.Text & " %"
        End If
    End Sub

    Private Sub DLSales_Total_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLSales_Total.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim PLAN_MONTHLY As Label = CType(e.Item.FindControl("Label20"), Label)
            Dim PLAN_YEARLY As Label = CType(e.Item.FindControl("Label21"), Label)

            Dim QTY_MONTHLY As Label = CType(e.Item.FindControl("Label18"), Label)
            Dim QTY_YEARLY As Label = CType(e.Item.FindControl("Label19"), Label)

            Dim sm As Label = CType(e.Item.FindControl("Label23"), Label)
            Dim sy As Label = CType(e.Item.FindControl("Label24"), Label)

            Try
                If CInt(PLAN_MONTHLY.Text) > 0 Then
                    sm.Text = String.Format("{0:n2}", QTY_MONTHLY.Text / PLAN_MONTHLY.Text * 100)
                Else
                    sm.Text = "0"
                End If
            Catch ex As Exception
                sm.Text = "0"
            End Try

            Try
                If CInt(PLAN_YEARLY.Text) > 0 Then
                    sy.Text = String.Format("{0:n2}", QTY_YEARLY.Text / PLAN_YEARLY.Text * 100)
                Else
                    sy.Text = "0"
                End If
            Catch ex As Exception
                sy.Text = "0"
            End Try

            If sm.Text >= 100 Then
                sm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sm.ForeColor = Drawing.Color.White
            ElseIf sm.Text > 79 And sm.Text < 100 Then
                sm.BackColor = Drawing.Color.Yellow
                sm.ForeColor = Drawing.Color.Black
            Else
                sm.ForeColor = Drawing.Color.White
                sm.BackColor = Drawing.Color.Red
            End If

            If sy.Text >= 100 Then
                sy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sy.ForeColor = Drawing.Color.White
            ElseIf sy.Text > 79 And sy.Text < 100 Then
                sy.BackColor = Drawing.Color.Yellow
                sy.ForeColor = Drawing.Color.Black
            Else
                sy.ForeColor = Drawing.Color.White
                sy.BackColor = Drawing.Color.Red
            End If


            sm.Text = sm.Text & " %"
            sy.Text = sy.Text & " %"
        End If
    End Sub

    Private Sub DLSales_Total4_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLSales_Total4.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim PLAN_MONTHLY As Label = CType(e.Item.FindControl("Label20"), Label)
            Dim PLAN_YEARLY As Label = CType(e.Item.FindControl("Label21"), Label)

            Dim QTY_MONTHLY As Label = CType(e.Item.FindControl("Label18"), Label)
            Dim QTY_YEARLY As Label = CType(e.Item.FindControl("Label19"), Label)

            Dim sm As Label = CType(e.Item.FindControl("Label22"), Label)
            Dim sy As Label = CType(e.Item.FindControl("Label23"), Label)

            Try
                If CInt(PLAN_MONTHLY.Text) > 0 Then
                    sm.Text = String.Format("{0:n2}", QTY_MONTHLY.Text / PLAN_MONTHLY.Text * 100)
                Else
                    sm.Text = "0"
                End If
            Catch ex As Exception
                sm.Text = "0"
            End Try

            Try
                If CInt(PLAN_YEARLY.Text) > 0 Then
                    sy.Text = String.Format("{0:n2}", QTY_YEARLY.Text / PLAN_YEARLY.Text * 100)
                Else
                    sy.Text = "0"
                End If
            Catch ex As Exception
                sy.Text = "0"
            End Try

            If sm.Text >= 100 Then
                sm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sm.ForeColor = Drawing.Color.White
            ElseIf sm.Text > 79 And sm.Text < 100 Then
                sm.BackColor = Drawing.Color.Yellow
                sm.ForeColor = Drawing.Color.Black
            Else
                sm.ForeColor = Drawing.Color.White
                sm.BackColor = Drawing.Color.Red
            End If

            If sy.Text >= 100 Then
                sy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sy.ForeColor = Drawing.Color.White
            ElseIf sy.Text > 79 And sy.Text < 100 Then
                sy.BackColor = Drawing.Color.Yellow
                sy.ForeColor = Drawing.Color.Black
            Else
                sy.ForeColor = Drawing.Color.White
                sy.BackColor = Drawing.Color.Red
            End If


            sm.Text = sm.Text & " %"
            sy.Text = sy.Text & " %"
        End If
    End Sub


    Private Sub DLSalesAll_Total_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DLSalesAll_Total.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim PLAN_MONTHLY As Label = CType(e.Item.FindControl("Label20"), Label)
            Dim PLAN_YEARLY As Label = CType(e.Item.FindControl("Label21"), Label)

            Dim QTY_MONTHLY As Label = CType(e.Item.FindControl("Label18"), Label)
            Dim QTY_YEARLY As Label = CType(e.Item.FindControl("Label19"), Label)

            Dim sm As Label = CType(e.Item.FindControl("Label22"), Label)
            Dim sy As Label = CType(e.Item.FindControl("Label23"), Label)

            Try
                If CInt(PLAN_MONTHLY.Text) > 0 Then
                    sm.Text = String.Format("{0:n2}", QTY_MONTHLY.Text / PLAN_MONTHLY.Text * 100)
                Else
                    sm.Text = "0"
                End If
            Catch ex As Exception
                sm.Text = "0"
            End Try

            Try
                If CInt(PLAN_YEARLY.Text) > 0 Then
                    sy.Text = String.Format("{0:n2}", QTY_YEARLY.Text / PLAN_YEARLY.Text * 100)
                Else
                    sy.Text = "0"
                End If
            Catch ex As Exception
                sy.Text = "0"
            End Try

            If sm.Text >= 100 Then
                sm.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sm.ForeColor = Drawing.Color.White
            ElseIf sm.Text > 79 And sm.Text < 100 Then
                sm.BackColor = Drawing.Color.Yellow
                sm.ForeColor = Drawing.Color.Black
            Else
                sm.ForeColor = Drawing.Color.White
                sm.BackColor = Drawing.Color.Red
            End If

            If sy.Text >= 100 Then
                sy.BackColor = Drawing.Color.FromArgb(34, 150, 14)
                sy.ForeColor = Drawing.Color.White
            ElseIf sy.Text > 79 And sy.Text < 100 Then
                sy.BackColor = Drawing.Color.Yellow
                sy.ForeColor = Drawing.Color.Black
            Else
                sy.ForeColor = Drawing.Color.White
                sy.BackColor = Drawing.Color.Red
            End If


            sm.Text = sm.Text & " %"
            sy.Text = sy.Text & " %"
        End If
    End Sub
End Class